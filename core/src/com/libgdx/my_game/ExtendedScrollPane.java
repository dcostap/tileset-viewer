package com.libgdx.my_game;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/** Custom ScrollPane with more options and some fixes */
public class ExtendedScrollPane extends ScrollPane {
    private ClickListener clickListener;

    /** will allow the Pane to catch mouse wheel input to scroll, even if not on focus */
    private boolean scrollWhenMouseIsOver;

    private boolean avoidShrinkOnHeight, avoidShrinkOnWidth;

    /** prevents over-scrolling, fading of bars... */
    private boolean optionsForComputerScrolling;

    /**@param avoidShrinkOnHeight Also disables the scrolling bar on Y
     * @param avoidShrinkOnWidth Also disables the scrolling bar on X */
    public ExtendedScrollPane(Actor widget, Skin skin, boolean scrollWhenMouseIsOver, boolean avoidShrinkOnHeight,
                              boolean avoidShrinkOnWidth, boolean optionsForComputerScrolling) {
        super(widget, skin);
        this.scrollWhenMouseIsOver = scrollWhenMouseIsOver;
        this.avoidShrinkOnHeight = avoidShrinkOnHeight;
        this.avoidShrinkOnWidth = avoidShrinkOnWidth;
        this.optionsForComputerScrolling = optionsForComputerScrolling;

        clickListener = new ClickListener(Input.Buttons.LEFT);
        addListener(clickListener);

        if (optionsForComputerScrolling) {
            this.setOverscroll(false, false);
            this.setFlickScroll(false);
            this.setFadeScrollBars(false);
        }

        this.setScrollingDisabled(avoidShrinkOnWidth, avoidShrinkOnHeight);

        // fix for when a Slider is inside the ScrollPane
        // pane won't move when you touch the slider
        addCaptureListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Actor actor = ExtendedScrollPane.this.hit(x, y, true);
                if (actor instanceof Slider) {
                    ExtendedScrollPane.this.setFlickScroll(false);
                    return true;
                }

                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                ExtendedScrollPane.this.setFlickScroll(true);
                super.touchUp(event, x, y, pointer, button);
            }
        });
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (scrollWhenMouseIsOver) {
            if (clickListener.isOver()) {
                getStage().setScrollFocus(this);
            } else if (getStage().getScrollFocus() == this) {
                getStage().setScrollFocus(null);
            }
        }
    }

    /** by default it returns 0 in getMinHeight(). This causes the widget to shrink completely when it can't expand
     * These booleans override the behavior */
    @Override
    public float getMinHeight() {
        if (!avoidShrinkOnHeight)
            return super.getMinHeight();
        else
            return getPrefHeight();
    }

    @Override
    public float getMinWidth() {
        if (!avoidShrinkOnWidth)
            return super.getMinWidth();
        else
            return getPrefWidth();
    }
}