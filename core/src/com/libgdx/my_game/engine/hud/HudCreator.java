package com.libgdx.my_game.engine.hud;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.libgdx.my_game.Assets;
import com.libgdx.my_game.MyGame;
import com.libgdx.my_game.utils.Updatable;

/**
 * Created by Darius on 20/11/2017
 * <p>
 * HudCreators create, update & draw HUD, above HudController's base hud variables.
 * Needs to be updated. HUD is drawn in HudController.
 */
public abstract class HudCreator implements Updatable {
    protected HudController hudController;
    protected MyGame game;

    protected Skin skin;
    protected Assets assets;

    public HudCreator(HudController hudController, MyGame game) {
        this.hudController = hudController;
        this.game = game;

        this.skin = hudController.getSkin();
        this.assets = game.getAssets();
    }

    public abstract void createHud();

    public abstract void update(float delta);

    public void deleteHud() {
        hudController.resetBaseHud();
    }
}
