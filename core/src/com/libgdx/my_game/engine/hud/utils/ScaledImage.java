package com.libgdx.my_game.engine.hud.utils;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.libgdx.my_game.utils.GameDrawer;

/**
 * Created by Darius on 27/11/2017
 * <p>
 * Scene2d Image that scales based on a base size and on the application's density factor
 * Same base sizes = same physical sizes on screens
 */
public class ScaledImage extends Image {
    public ScaledImage(TextureRegion region, float size) {
        super(region);
        this.validate();

        int scaleFactor = GameDrawer.getImageScaleFactor(size, this.getImageWidth(), this.getImageHeight());

        this.getDrawable().setMinWidth(getImageWidth() * scaleFactor);
        this.getDrawable().setMinHeight(getImageHeight() * scaleFactor);
    }
}
