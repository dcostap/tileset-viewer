package com.libgdx.my_game.engine.hud.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.libgdx.my_game.MyGame;

/**
 * Created by Darius on 24/11/2017
 */
public class LabelButton extends Button {
    private static final int defaultPadding = 10;

    Label label;

    public LabelButton(Label label, Skin skin, int padding) {
        super(skin);

        this.label = label;
        this.add(label);
        this.pad(padding * MyGame.getDensityFactor());
    }

    /**
     * Doesn't use LabelStyle. Uses ExtendedLabel so directly specify BitmapFont and its color
     *
     * @param font      BitmapFont used
     * @param fontColor color of the font
     * @param skin      skin for the button
     */
    public LabelButton(String text, BitmapFont font, Color fontColor, Skin skin, int padding) {
        this(new ExtendedLabel(text, font, fontColor), skin, padding);
    }

    /**
     * Uses default padding
     *
     * @see #LabelButton(String, BitmapFont, Color, Skin, int)
     */
    public LabelButton(String text, BitmapFont font, Color fontColor, Skin skin) {
        this(new ExtendedLabel(text, font, fontColor), skin, defaultPadding);
    }

    /**
     * Uses ExtendedLabel's default color for the font
     *
     * @see #LabelButton(String, BitmapFont, Color, Skin, int)
     */
    public LabelButton(String text, BitmapFont font, Skin skin, int padding) {
        this(new ExtendedLabel(text, font), skin, padding);
    }

    /**
     * Default Color and default padding
     */
    public LabelButton(String text, BitmapFont font, Skin skin) {
        this(new ExtendedLabel(text, font), skin, defaultPadding);
    }

    public void setText(CharSequence newText) {
        label.setText(newText);
    }

    /**
     * Uses default font inside skin, on LabelStyle
     */
    public LabelButton(String text, Skin skin) {
        this(new Label(text, skin), skin, 10);
    }

    public LabelButton(String text, Skin skin, int padding) {
        this(new Label(text, skin), skin, padding);
    }

    public LabelButton(String text, Skin skin, String styleName, int padding) {
        this(new Label(text, skin, styleName), skin, padding);
    }
}
