package com.libgdx.my_game.engine.hud.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;

import javax.swing.*;

/**
 * Created by Darius on 27/11/2017
 * <p>
 * Label that doesn't use LabelStyle. Directly specify BitmapFont and its color.
 * No need to manually include into the skin all the fonts if you use this Label :D
 */
public class ExtendedLabel extends Label {
    public ExtendedLabel(String text, BitmapFont font, Color fontColor) {
        super(text, new LabelStyle(font, fontColor));
    }

    public ExtendedLabel(String text, BitmapFont font, Color fontColor, int alignment) {
        this(text, font, fontColor);
        setAlignment(alignment);
    }

    /**
     * No color specified = uses font's default color (color chosen when created)
     */
    public ExtendedLabel(String text, BitmapFont font) {
        this(text, font, Color.BLACK);
    }

    public void setFontColor(Color newColor) {
        if (!newColor.equals(getStyle().fontColor)) {
            getStyle().fontColor = newColor;
        }
    }

    public void setFontAlpha(float alpha) {
        getStyle().fontColor.a = alpha;
    }

    public float getFontAlpha() {
        return getStyle().fontColor.a;
    }
}
