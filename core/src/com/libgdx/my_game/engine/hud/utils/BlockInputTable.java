package com.libgdx.my_game.engine.hud.utils;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by Darius on 24/11/2017
 * <p>
 * Blocks all input. Input blocked will not be propagated to Actors below or other InputProcessors
 * Is set to FillParent by default.
 */
public class BlockInputTable extends Table {
    public BlockInputTable() {
        this.setTouchable(Touchable.enabled);
        this.setFillParent(true);

        addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {

            }
        });
    }
}
