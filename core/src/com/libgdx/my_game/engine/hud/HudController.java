package com.libgdx.my_game.engine.hud;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.libgdx.my_game.Assets;
import com.libgdx.my_game.utils.GameDrawer;
import com.libgdx.my_game.engine.hud.utils.BlockInputTable;
import com.libgdx.my_game.engine.hud.utils.ExtendedLabel;
import com.libgdx.my_game.utils.Drawable;
import com.libgdx.my_game.utils.Utils;
import com.libgdx.my_game.utils.input.TouchInputListener;
import com.libgdx.my_game.utils.input.TouchInputNotifier;

import static com.libgdx.my_game.utils.Utils.centerActor;
import static com.libgdx.my_game.utils.Utils.mapNumberToRange;

/**
 * Created by Darius on 19/11/2017
 * <p>
 * Creates & manages a base HUD layout. Holds the Scene2d Stage.
 */
public class HudController implements Drawable, TouchInputListener {
    public Stage stage;
    public Table mainTable;
    public Table upTable;
    public Table centerLeftTable;
    public Table centerRightTable;
    public Table downTable;
    private Skin skin;
    private Window debugSelectionWindow;
    private Window popUpWindow;
    private float showSelectionWindowTimer = 0;

    private BlockInputTable blockInputTable;

    private Assets assets;

    public HudController(TouchInputNotifier touchInputNotifier, Assets assets, Stage stage) {
        this.stage = stage;
        this.assets = assets;
        this.skin = assets.skin;

        createBaseHud();
        createDebugHud();

        touchInputNotifier.registerObserver(this);
    }

    @Override
    public void draw(GameDrawer gameDrawer, float delta) {
        updateHudDebug(delta);

        stage.act(delta);

        gameDrawer.getBatch().setProjectionMatrix(stage.getCamera().combined);
        gameDrawer.getBatch().begin();

        // draw GameDrawer stack of text
        if (gameDrawer.hasStackOfTextFromWorldCoordsToDraw()) {
            gameDrawer.drawStackOfTextFromWorldCoords(gameDrawer.getAssets().fontDefault.font_small);
        }

        gameDrawer.getBatch().end();

        stage.draw();
    }

    private void createBaseHud() {
        stage.setDebugAll(false);

        mainTable = new Table();
        mainTable.setFillParent(true);

        upTable = new Table();
        centerLeftTable = new Table();
        centerRightTable = new Table();
        downTable = new Table();

        mainTable.add(upTable).expandX().fill().top().colspan(2);
        mainTable.row();
        Table centerTable = new Table();
        mainTable.add(centerTable).expand().fill();
        centerTable.add(centerLeftTable).left().expand().fill();
        centerTable.add(centerRightTable).right();
        mainTable.row();
        mainTable.add(downTable).expand().fill().bottom().colspan(2);

        stage.addActor(mainTable);

        // table that will block input when needed
        blockInputTable = new BlockInputTable();
        blockInputTable.setVisible(false);
        stage.addActor(blockInputTable);
    }

    public void resetBaseHud() {
        upTable.clearChildren();
        centerLeftTable.clearChildren();
        centerRightTable.clearChildren();
        downTable.clearChildren();
    }

    private void createDebugHud() {
        debugSelectionWindow = new Window("", skin);
        debugSelectionWindow.setVisible(false);
        stage.addActor(debugSelectionWindow);

        Utils.modifyScene2dActorToBlockInputBeneathItself(debugSelectionWindow);
    }

    /**
     * Automatically adds a window to the stage, adding a BlockInputTable to block input around the Window
     * Automatically modifies the Window to catch input (so it's not propagated beneath it - but children can catch it)
     * Automatically closes any other popUp-windows, and places the window in the center of the screen
     * Use this instead of manually adding a Window to the stage, if you want those features
     */
    public void showPopUpWindow(Window popUpWindow) {
        hidePopUpWindow();
        this.popUpWindow = popUpWindow;
        stage.addActor(popUpWindow); // important that each Window is added after blockInputTable
        popUpWindow.pack();

        // center the window
        centerActor(popUpWindow, stage);

        // to block inputs around the Window...
        blockInputTable.setVisible(true);

        // to block inputs beneath the Window...
        Utils.modifyScene2dActorToBlockInputBeneathItself(popUpWindow);
    }

    /**
     * Hides the popUp-window, if there's any
     */
    public void hidePopUpWindow() {
        if (popUpWindow != null) {
            popUpWindow.remove();
            popUpWindow = null;
        }

        blockInputTable.setVisible(false);
    }

    /**
     * DebugSelectionWindow is used for debug purposes, so it's maintained by the main HudController
     */
    public void showDebugSelectionWindow(Table contents) {
        debugSelectionWindow.clearChildren();

        debugSelectionWindow.add(new ExtendedLabel("Debug info", assets.fontDefault.font_small));
        debugSelectionWindow.row();
        debugSelectionWindow.add(contents);
        debugSelectionWindow.pack();

        debugSelectionWindow.setVisible(true);
        debugSelectionWindow.setPosition(5, 20);
        debugSelectionWindow.setMovable(true);

        showSelectionWindowTimer = 0.3f;
    }

    private void updateHudDebug(float delta) {
        showSelectionWindowTimer -= delta;
        if (showSelectionWindowTimer < 0) showSelectionWindowTimer = 0;
    }

    public Skin getSkin() {
        return skin;
    }

    @Override
    public void touchDownEvent(float screenX, float screenY, float worldX, float worldY, int button, int pointer, boolean isJustPressed) {

    }

    @Override
    public void touchReleasedEvent(float screenX, float screenY, float worldX, float worldY, int button, int pointer) {
        if (showSelectionWindowTimer > 0) return;

        debugSelectionWindow.setVisible(false);
    }
}
