package com.libgdx.my_game.engine.map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;
import com.libgdx.my_game.utils.GameDrawer;
import com.libgdx.my_game.MyGame;
import com.libgdx.my_game.engine.map.entities.Entity;
import com.libgdx.my_game.engine.map.entities.CollidingEntity;
import com.libgdx.my_game.utils.Drawable;
import com.libgdx.my_game.utils.FlashingThing;

/**
 * Created by Darius on 14/09/2017.
 */
public class MapCell implements Drawable {
    public Array<Entity> tiledEntities = new Array<>();
    public Node node;
    private int x, y;
    private boolean hasSolid = false;
    private int cellSize;
    private com.libgdx.my_game.engine.map.EntityTiledMap map;
    private FlashingThing flashingThing = new FlashingThing();

    private com.libgdx.my_game.engine.map.Tile tile = new com.libgdx.my_game.engine.map.Tile();

    public MapCell(int x, int y, int cellSize, com.libgdx.my_game.engine.map.EntityTiledMap map) {
        this.x = x;
        this.y = y;
        this.cellSize = cellSize;
        this.map = map;

        this.node = new Node();
    }

    public FlashingThing getFlashingThing() {
        return flashingThing;
    }

    /** Whether any static solid Entity occupies this cell. May be used to speed up collision detection.
     * @see CollidingEntity */
    public boolean hasSolid() {
        return hasSolid;
    }

    public void updateHasSolid() {
        hasSolid = false;
        for (Entity ent : tiledEntities) {
            if (ent.isSolid()) {
                hasSolid = true;
                return;
            }
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public float getMiddleX() {
        return x + ((float) cellSize / 2f);
    }

    public float getMiddleY() {
        return y + ((float) cellSize / 2f);
    }

    public void draw(GameDrawer gameDrawer, float delta) {
        if (tile.tileSpr != null) {
            gameDrawer.draw(tile.tileSpr, x, y, tile.rotation);
        }

        if (hasSolid() && MyGame.DEBUG) {
            gameDrawer.setAlpha(0.06f);
            gameDrawer.setColor(Color.BLACK);
            gameDrawer.drawRectangle(x, y, 1, 1, 0, true);
            gameDrawer.resetAlpha();
            gameDrawer.resetColor();
        }

        flashingThing.update(delta);

        if (flashingThing.isFlashing()) {
            flashingThing.setupGameDrawer(gameDrawer);

            gameDrawer.drawRectangle(x, y, 1, 1, 0, true);

            flashingThing.resetGameDrawer(gameDrawer);
        }
    }

    // return suitable neighbors of current cell when pathfinding
    // here you must check the neighbor isn't out of bounds (so it doesn't exist)
    // and that the neighbor isn't solid
    public Array<MapCell> getNeighbors(MapCell finish, Entity ignoredEntity, boolean allowNonWalkableCells) {
        Array<MapCell> neighbors = new Array<>();

        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                if (x == 0 && y == 0)
                    continue;

                int xx, yy;
                xx = this.x + x;
                yy = this.y + y;

                if (map.isInsideMap(xx, yy)) {

                    MapCell current = map.getMapCell(xx, yy);

                    // is current the goal node? return a list with only that node
                    if (current == finish) {
                        Array<MapCell> l = new Array<>();
                        l.add(current);
                        return l;
                    }

                    if (allowNonWalkableCells || isCellValid(xx, yy, ignoredEntity)) {
                        boolean valid = true;

                        // find diagonal neighbors and discard invalid ones
                        if (!allowNonWalkableCells && xx != this.x && yy != this.y) {
                            // check if the diagonal block is not surrounded by 1 or 2 solids

                            if (xx < this.x && yy > this.y) {
                                if (!isCellValid(xx + 1, yy, ignoredEntity) || !isCellValid(xx, yy - 1, ignoredEntity))
                                    valid = false;
                            } else if (xx > this.x && yy > this.y) {
                                if (!isCellValid(xx - 1, yy, ignoredEntity) || !isCellValid(xx, yy - 1, ignoredEntity))
                                    valid = false;
                            } else if (xx < this.x && yy < this.y) {
                                if (!isCellValid(xx + 1, yy, ignoredEntity) || !isCellValid(xx, yy + 1, ignoredEntity))
                                    valid = false;
                            } else if (xx > this.x && yy < this.y) {
                                if (!isCellValid(xx - 1, yy, ignoredEntity) || !isCellValid(xx, yy + 1, ignoredEntity))
                                    valid = false;
                            }
                        }

                        if (valid) {

                            neighbors.add(current);

                            //System.out.println("added neighbor - ");

                            // put the values really high if they were not initialized yet
                            if (current.node.f == -1) {
                                current.node.f = Float.POSITIVE_INFINITY;
                                current.node.g = Float.POSITIVE_INFINITY;
                            }
                        }
                    }
                }
            }
        }

        return neighbors;
    }

    private boolean isCellValid(int x, int y, Entity ignoredEntity) {
        if (!map.isInsideMap(x, y)) return false;

        MapCell cell = map.getMapCell(x, y);
        return (cell.hasSolid || (ignoredEntity != null && cell.tiledEntities.contains(ignoredEntity, true)));
    }

    public com.libgdx.my_game.engine.map.Tile getTile() {
        return tile;
    }

    public class Node {
        public float g, f;
        public MapCell cameFrom;

        public Node() {
            reset();
        }

        // after performing a pathfinding algorithm, reset these values
        public void reset() {
            g = -1f;
            f = -1f;
            cameFrom = null;
        }
    }
}
