package com.libgdx.my_game.engine.map;


import com.libgdx.my_game.engine.map.entities.Entity;

import java.util.Comparator;

/**
 * Created by Darius on 28/10/2017.
 */
public class EntityComparatorByYPosition implements Comparator<Entity> {
    @Override
    public int compare(Entity o1, Entity o2) {
        // entities ordered based on y-position
        return (Float.compare(o2.getBoundingBox().y, o1.getBoundingBox().y));
    }
}
