package com.libgdx.my_game.engine.map.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.libgdx.my_game.utils.GameDrawer;
import com.libgdx.my_game.MyGame;
import com.libgdx.my_game.engine.map.EntityTiledMap;
import com.libgdx.my_game.utils.Drawable;
import com.libgdx.my_game.utils.FlashingThing;
import com.libgdx.my_game.utils.Updatable;

/**
 * Created by Darius on 14/09/2017.
 * <p>
 * Entities are instances of the game world, stored and updated on EntityTiledMap.
 */
public abstract class Entity implements Drawable, Updatable {
    private Rectangle boundingBox;
    private Vector2 position;
    private EntityTiledMap map;

    private boolean isStatic;
    private boolean isSolid;
    private boolean getsTiledToCell;

    private Rectangle absoluteBoundingBox;

    private boolean killed = false;
    private boolean hasMoved = false;

    private Array<Entity> possibleCollisionArray = new Array<>();

    private FlashingThing flashingThing = new FlashingThing();

    private Array<Entity> dummyEntityArray = new Array<>();

    /**
     * @param boundingBox     Use pixel units in coordinates, translated to game units in the constructor.
     *                        x, y values are the offset of the bounding box.
     * @param isSolid         Flag that may be used in collision response. Generally means that solid entities should not
     *                        be overlapped.
     * @param isStatic        Static entities can't move from initial position.
     *                        If static, it will be included in map's quadTree as a static entity.
     *                        If not static, will be included in map's quadTree as a dynamic entity.
     *                        If solid, mapCells occupied will be marked as solid.
     * @param getsTiledToCell Only applies to static entities. If true, entity will be included in a list in affected cells.
     */
    public Entity(Vector2 position, Rectangle boundingBox, EntityTiledMap map, boolean isSolid, boolean isStatic, boolean getsTiledToCell) {
        this.position = position;
        this.map = map;

        this.isSolid = isSolid;
        this.isStatic = isStatic;
        this.getsTiledToCell = isStatic && getsTiledToCell;

        // translate bounding box pixels to units
        boundingBox.x /= MyGame.PPM;
        boundingBox.y /= MyGame.PPM;

        // extra translation to units -> width and height
        boundingBox.width /= MyGame.PPM;
        boundingBox.height /= MyGame.PPM;

        // bounding box size is reduced a bit to avoid BBs with size of 1 unit to occupy 2 cells, when the BB is snapped to the grid
        // (because when checking for occupied cells, if BB is at position 2, 2 with size of 1, 1; the algorithm will
        // return cells 2, 2 and (2 + 1), (2 + 1) as occupied. But BB of size 1 is normally meant to occupy 1 cell only; this
        // fixes it)
        float bbSizeMargin = 0.01f;
        boundingBox.width -= bbSizeMargin;
        boundingBox.height -= bbSizeMargin;

        this.boundingBox = boundingBox;

        absoluteBoundingBox = new Rectangle(0, 0, 0, 0);
    }

    /**
     * Standard constructor
     */
    public Entity(Vector2 position, Rectangle boundingBox, EntityTiledMap map, boolean isSolid, boolean isStatic) {
        this(position, boundingBox, map, isSolid, isStatic, true);
    }

    /**
     * Automatic bounding box of 1 cell = 1 unit
     */
    public Entity(Vector2 position, EntityTiledMap map, boolean isSolid, boolean isStatic) {
        this(position, new Rectangle(0, 0, MyGame.PPM, MyGame.PPM), map, isSolid, isStatic);
    }

    /**
     * If relative and using delta, will move "units specified" /s.
     *
     * @param relative If not relative, delta is ignored
     * @param delta    Use value 1 to ignore delta and move the exact quantity specified
     */
    public final void move(float x, float y, boolean relative, float delta) {
        if (isStatic)
            throw new UnsupportedOperationException("Tried to move a static entity: " + getClass().getSimpleName());

        if (relative) {
            x *= delta;
            y *= delta;

            if (x == 0 && y == 0) {
                hasMoved = false;
                return;
            }

            position.x += x;
            position.y += y;
        } else {
            if (x == position.x && y == position.y) {
                hasMoved = false;
                return;
            }
            position.x = x;
            position.y = y;
        }

        hasMoved = true;
    }

    public final boolean isKilled() {
        return killed;
    }

    /**
     * "killed" entities must be completely ignored and removed from lists so that GC can remove the instance.
     */
    public void kill() {
        map.removeEntity(this);
        killed = true;
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void draw(GameDrawer gameDrawer, float delta) {
        if (MyGame.DEBUG)
            drawDebug(gameDrawer, delta);
    }

    protected void drawDebug(GameDrawer gameDrawer, float delta) {
        gameDrawer.setAlpha(0.6f);
        gameDrawer.setColor(isStatic ? Color.RED : Color.BLUE);
        gameDrawer.drawRectangle(getBoundingBox(), 0.03f, false);

        if (isSolid) {
            gameDrawer.setAlpha(0.1f);
            float marginx = getBoundingBox().width / 5f;
            float marginy = getBoundingBox().height / 5f;
            gameDrawer.drawRectangle(getBoundingBox().x + marginx, getBoundingBox().y + marginy,
                    getBoundingBox().width - marginx * 2, getBoundingBox().height - marginy * 2, 0.06f, false);
        }

        gameDrawer.setColor(Color.CHARTREUSE);
        gameDrawer.drawRectangle(getPosition().x - 0.1f, getPosition().y - 0.1f, 0.2f, 0.2f, 0, true);

        gameDrawer.resetAlpha();
        gameDrawer.resetColor();

        flashingThing.update(delta);

        if (flashingThing.isFlashing()) {
            flashingThing.setupGameDrawer(gameDrawer);

            gameDrawer.drawRectangle(getBoundingBox(), 0, true);

            flashingThing.resetGameDrawer(gameDrawer);
        }
    }

    /**
     * @return The absolute positioned Rectangle that represents the bounding box
     */
    public final Rectangle getBoundingBox() {
        absoluteBoundingBox.x = position.x + boundingBox.x;
        absoluteBoundingBox.y = position.y + boundingBox.y;
        absoluteBoundingBox.width = boundingBox.width;
        absoluteBoundingBox.height = boundingBox.height;

        return absoluteBoundingBox;
    }

    /**
     * Warning: modification of this bounding box on static entities will lead to bugs
     *
     * @return The internal bounding box, represented as a Rectangle relative to the position
     */
    public final Rectangle getRelativeBoundingBox() {
        return boundingBox;
    }

    /**
     * Call each frame to update the list of possible colliding entities.
     * <ul>
     * Call this method when:
     * <li>entities might have been added / removed </li>
     * <li>when dynamic entities, including this one, might have moved </li>
     * </ul>
     */
    public final void updateCollidingState(boolean includeDynamicEntities) {
        possibleCollisionArray.clear();

        for (Entity entity : map.getCollisionTree().getPossibleCollidingEntities(this.getBoundingBox(), includeDynamicEntities)) {
            if (entity == this) continue;
            possibleCollisionArray.add(entity);
        }
    }

    public final boolean isCollidingWithSpecificEntity(Entity otherEntity) {
        return getBoundingBox().overlaps(otherEntity.getBoundingBox());
    }

    /**
     * Use this to check for solid collision without having to do the expensive call to the CollisionTree
     * If the Entity will only collide with static solids, and the solid entities are well placed in the cells <p/>
     * Limitations compared to checking collision against Entities (using {@link #updateCollidingState(boolean)}): <ul>
     * <li>Solid collision is only done against mapCell's boundaries, no precise collision checking on arbitrary positions </li>
     * <li>Only affected by (solid & tiled) Entities (which modify the mapCells)</li>
     * </ul>
     */
    public final boolean isCollidingWithSolidMapCell() {
        for (com.libgdx.my_game.engine.map.MapCell mapCell : map.getCellsOccupiedByRectangle(getBoundingBox())) {
            if (mapCell.hasSolid()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Uses array of possible colliding Entities, so it may be outdated if updateCollidingState wasn't called since
     * the Entity last moved - or since any other Entity moved <p />
     * Don't keep references to returned Array!
     *
     * @return All Entities found that are of the specified class and that collide with this Entity
     */
    public final Array<Entity> isCollidingWithAnyEntityOfClass(Class vClass) {
        dummyEntityArray.clear();

        for (Entity entity : getPossibleCollidingEntities()) {
            if (vClass.isInstance(entity) && isCollidingWithSpecificEntity(entity)) {
                dummyEntityArray.add(entity);
            }
        }

        return dummyEntityArray;
    }

    /**
     * Update the collision state if the Entity moved, to update the returned Array
     *
     * @return Array of Entities that might collide with this Entity. (Entities that are in the same CollisionTree-cell)
     */
    public final Array<Entity> getPossibleCollidingEntities() {
        return possibleCollisionArray;
    }

    public final boolean hasMoved() {
        return hasMoved;
    }

    /**
     * Call on all Entities when a new update loop starts, since this variable is meant to show if the Entity
     * has moved since the loop started
     */
    public final void resetHasMoved() {
        hasMoved = false;
    }

    public final EntityTiledMap getMap() {
        return map;
    }

    public final Vector2 getPosition() {
        return position;
    }

    public final float getX() {
        return position.x;
    }

    public final float getY() {
        return position.y;
    }

    public final void getTiledPosition(Vector2 vectorToModify) {
        vectorToModify.set((int) Math.floor(position.x), (int) Math.floor(position.y));
    }

    public final void getTiledPosition(GridPoint2 gridPointToModify) {
        gridPointToModify.set((int) Math.floor(position.x), (int) Math.floor(position.y));
    }

    public final int getTiledX() {
        return (int) Math.floor(position.x);
    }

    public final int getTiledY() {
        return (int) Math.floor(position.y);
    }

    /**
     * Entities can specify whether they are "valid" or not. Override on subclasses if needed.
     * AI algorithms will avoid interacting with invalid entities
     */
    public boolean isValid() {
        return (!isKilled());
    }

    public final boolean isStatic() {
        return isStatic;
    }

    public final boolean isSolid() {
        return isSolid;
    }

    public final boolean getsTiledToCell() {
        return getsTiledToCell;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + (isSolid ? "solid" : "not solid") + ", "
                + (isStatic ? "static" : "not static") + ", " + (killed ? "killed" : "not killed") + ")"
                + "; hash: " + hashCode();
    }

    public final FlashingThing getDebugEntityFlashingThing() {
        return flashingThing;
    }

    protected final void scaleBoundingBox(float scaleFactor) {
        boundingBox.x *= scaleFactor;
        boundingBox.y *= scaleFactor;
        boundingBox.width *= scaleFactor;
        boundingBox.height *= scaleFactor;
    }
}
