package com.libgdx.my_game.engine.map.entities;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.libgdx.my_game.engine.map.MapCell;
import com.libgdx.my_game.utils.GameDrawer;

/**
 * Created by Darius on 15/09/2017.
 *
 * <p /> Provides collision response against certain Entities, define which in {@link #doCollisionResponseWithThisEntity(Entity)}
 * <p /> Provides speed variable for movement. Collision response will be done when calling {@link #update(float)}.
 *
 * During collision response the Entity will move the quantity specified in the speed variable, and check for collisions
 * against the specified Entities; if collisions exist, this Entity will move out of the collision.
 *
 * <p /> Note that the Entity's colliding state will be updated while doing collision response, so any subclass might avoid
 * calling it again after.
 *
 * <p /> If the collision response will only be done against solid static Entities, consider setting {@link #onlyCheckAgainstSolidMapCells}
 */
public abstract class CollidingEntity extends Entity {

    /** precision, in game units, of collision detection */
    protected float precision = 0.01f;

    protected Vector2 speed = new Vector2();
    private boolean hasCollided = false;
    private int collidedX = 0;
    private int collidedY = 0;
    private Array<Entity> collidingEntitiesX = new Array<>();
    private Array<Entity> collidingEntitiesY = new Array<>();

    /** default: false; set it to true so that collisionResponse is done without using CollisionTree (less expensive) <p />
     * if true the collisionResponse will only check for MapCells marked as solid; it will therefore ignore Entities.
     * @see MapCell */
    protected boolean onlyCheckAgainstSolidMapCells = false;

    public final Vector2 getSpeed() {
        return speed;
    }

    public final void setSpeed(Vector2 speed) {
        this.speed = speed;
    }

    public final void setSpeed(float x, float y) {
        this.speed.set(x, y);
    }

    public CollidingEntity(Vector2 position, Rectangle boundingBox, com.libgdx.my_game.engine.map.EntityTiledMap map, boolean isSolid) {
        super(position, boundingBox, map, isSolid, false);
    }

    public CollidingEntity(Vector2 position, Rectangle boundingBox, com.libgdx.my_game.engine.map.EntityTiledMap map, boolean isSolid, boolean getsTiledToCell) {
        super(position, boundingBox, map, isSolid, false, getsTiledToCell);
    }

    @Override
    public void draw(GameDrawer gameDrawer, float delta) {
        super.draw(gameDrawer, delta);
    }

    @Override
    public void update(float delta) {
        moveColliding(speed.x, speed.y, delta);
    }

    public void moveColliding(float xAdd, float yAdd, float delta) {
        if (xAdd == 0 && yAdd == 0) return;

        super.move(xAdd, yAdd, true, delta);

        // reset collision information flags
        collidedX = 0;
        collidedY = 0;
        hasCollided = false;
        collidingEntitiesX.clear();
        collidingEntitiesY.clear();

        // collision response with collision tree
        if (!onlyCheckAgainstSolidMapCells) {
            updateCollidingState(true);

            if (isCollidingWithOneEntityValidForCollisionResponse(delta)) {
                hasCollided = true;
                fixCollisionAgainstCollisionTree(xAdd, yAdd, precision, delta);
            }
        } else { // collision response with mapCells
            if (isCollidingWithSolidMapCell()) {
                hasCollided = true;
                fixCollisionAgainstMapCells(xAdd, yAdd, precision, delta);
            }
        }
    }

    private boolean isCollidingWithOneEntityValidForCollisionResponse(float delta) {
        for (Entity entity : getPossibleCollidingEntities()) {
            if (doCollisionResponseWithThisEntity(entity) && this.isCollidingWithSpecificEntity(entity)) {
                return true;
            }
        }

        return false;
    }

    /** Overwrite to change which Entities are picked to do collision response; default: solid Entities */
    protected boolean doCollisionResponseWithThisEntity(Entity entity) {
        return entity.isSolid();
    }

    private void findAllCollidingEntitiesValidForCollisionResponse(Array<Entity> arrayToPopulate) {
        for (Entity entity : getPossibleCollidingEntities()) {
            if (doCollisionResponseWithThisEntity(entity) && this.isCollidingWithSpecificEntity(entity)) {
                arrayToPopulate.add(entity);
            }
        }
    }

    private void fixCollisionAgainstCollisionTree(float xAdd, float yAdd, float STEP, float delta) {
        int signX = (int) Math.signum(xAdd);
        int signY = (int) Math.signum(yAdd);

        // go back to original position
        super.move(-xAdd, -yAdd, true, delta);

        updateCollidingState(true);

        // was already colliding with solid? go back like crazy
        if (isCollidingWithOneEntityValidForCollisionResponse(delta)) {
            int count = 0;

            findAllCollidingEntitiesValidForCollisionResponse(collidingEntitiesX);
            collidingEntitiesY.addAll(collidingEntitiesX);

            while (count < 100) {
                count++;
                super.move(-STEP * signX, -STEP * signY, true, 1);

                updateCollidingState(true);
                if (!isCollidingWithOneEntityValidForCollisionResponse(delta)) {
                    break;
                }
            }
        } else {
            float xAdded = 0;
            float yAdded = 0;
            boolean xEnded = signX == 0;
            boolean yEnded = signY == 0;

            // first move STEP on x, then on y. Stop one coordinate when it moved enough, or when collided
            while (true) {
                float xTotalMoved = (xAdd * signX * delta);
                float yTotalMoved = (yAdd * signY * delta);
                if (!xEnded) {
                    super.move(STEP * signX, 0, true, 1);
                    xAdded += STEP;

                    updateCollidingState(true);
                    boolean hasSolidCollision = isCollidingWithOneEntityValidForCollisionResponse(delta);

                    // stop when collided with solid or when you moved all the original quantity moved
                    if (hasSolidCollision || xAdded >= xTotalMoved) {
                        if (hasSolidCollision) { // stopped because of solid collision
                            collidedX = signX;
                            findAllCollidingEntitiesValidForCollisionResponse(collidingEntitiesX);

                            // go back before the collision
                            super.move(-STEP * signX, 0, true, 1);
                            updateCollidingState(true);
                        }

                        xEnded = true;
                    }
                }

                if (!yEnded) {
                    super.move(0, STEP * signY, true, 1);
                    yAdded += STEP;

                    updateCollidingState(true);

                    boolean hasSolidCollision = isCollidingWithOneEntityValidForCollisionResponse(delta);
                    if (hasSolidCollision || yAdded >= yTotalMoved )  {
                        if (hasSolidCollision) {
                            collidedY = signY;
                            findAllCollidingEntitiesValidForCollisionResponse(collidingEntitiesY);

                            super.move(0, -STEP * signY, true, 1);
                            updateCollidingState(true);
                        }

                        yEnded = true;
                    }
                }

                if (xEnded && yEnded) {
                    break;
                }
            }
        }
    }

    private void fixCollisionAgainstMapCells(float xAdd, float yAdd, float STEP, float delta) {
        int signX = (int) Math.signum(xAdd);
        int signY = (int) Math.signum(yAdd);

        // go back to original position
        super.move(-xAdd, -yAdd, true, delta);

        // was already colliding with solid? go back like crazy
        if (isCollidingWithSolidMapCell()) {
            int count = 0;
            while (count < 1000) {
                count++;
                super.move(-STEP * signX, -STEP * signY, true, 1);

                if (!isCollidingWithSolidMapCell()) {
                    break;
                }
            }
        } else {
            float xAdded = 0;
            float yAdded = 0;
            boolean xEnded = signX == 0;
            boolean yEnded = signY == 0;

            // first move STEP on x, then on y. Stop one coordinate when it moved enough, or when collided
            while (true) {
                float xTotalMoved = (xAdd * signX * delta);
                float yTotalMoved = (yAdd * signY * delta);
                if (!xEnded) {
                    super.move(STEP * signX, 0, true, 1);
                    xAdded += STEP;

                    boolean hasSolidCollision = isCollidingWithSolidMapCell();

                    // stop when collided with solid or when you moved all the original quantity moved
                    if (hasSolidCollision || xAdded >= xTotalMoved) {
                        if (hasSolidCollision) { // stopped because of solid collision
                            collidedX = signX;

                            // go back before the collision
                            super.move(-STEP * signX, 0, true, 1);
                        }

                        xEnded = true;
                    }
                }

                if (!yEnded) {
                    super.move(0, STEP * signY, true, 1);
                    yAdded += STEP;

                    boolean hasSolidCollision = isCollidingWithSolidMapCell();
                    if (hasSolidCollision || yAdded >= yTotalMoved )  {
                        if (hasSolidCollision) {
                            collidedY = signY;

                            super.move(0, -STEP * signY, true, 1);
                        }

                        yEnded = true;
                    }
                }

                if (xEnded && yEnded) {
                    break;
                }
            }
        }

    }

    /** Whether in this frame, after processing movement, the Entity had to fix a collision with solid entities */
    public final boolean hasCollided() {
        return hasCollided;
    }

    /** Whether in this frame it has done collision response in X coordinate with a entity
     * @return 0 if false, 1 if true going to the right, -1 if true going to the left */
    public int getCollidedX() {
        return collidedX;
    }

    /** Whether in this frame it has done collision response in Y coordinate with a solid entity
     * @return 0 if false, 1 if true going upwards, -1 if true going downwards */
    public int getCollidedY() {
        return collidedY;
    }

    /** List of solid entities that the object collided with in this frame, while resolving collision
     * Use it with getCollidedX to find out in which direction it collided with those entities <p />
     * Example: if getCollidedX() returns 0, (or hasCollided() returns false) this will never return anything since
     * there was no collision response in this coordinate. If it returns 1, check this method.
     * All those entities collided with this entity and were to its right.
     * <p /> Note that this applies to collision-response valid entities only, just like the other methods
     * related to collision response flags */
    public final Array<Entity> getCollidingEntitiesX() {
        return collidingEntitiesX;
    }

    public final Array<Entity> getCollidingEntitiesY() {
        return collidingEntitiesY;
    }
}
