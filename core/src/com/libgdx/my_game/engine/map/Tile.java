package com.libgdx.my_game.engine.map;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Darius on 11/01/2018
 */
public class Tile {
    public int rotation;
    public TextureRegion tileSpr;
}
