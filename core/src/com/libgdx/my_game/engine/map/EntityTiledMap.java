package com.libgdx.my_game.engine.map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pools;
import com.libgdx.my_game.engine.map.entities.Entity;
import com.libgdx.my_game.utils.GameDrawer;
import com.libgdx.my_game.MyGame;
import com.libgdx.my_game.utils.Drawable;
import com.libgdx.my_game.utils.notifier.Notifier;
import com.libgdx.my_game.utils.Updatable;
import com.libgdx.my_game.utils.Utils;

import java.util.Comparator;

/**
 * Created by Darius on 14/09/2017.
 * <p>
 * After creating the map object, call {@link #initMap(int, int, int)} to load basic variables
 */
public class EntityTiledMap implements Drawable, Updatable {
    private final int drawingCameraBoundsBorder = 2;
    private Array<Entity> entityList = new Array<>();
    private Array<Entity> dynamicEntityList = new Array<>();
    private Array<Entity> toBeRemoved = new Array<>();
    private Array<Entity> toBeAdded = new Array<>();
    private ObjectMap<GridPoint2, MapCell> mapCells;
    private int mapWidth, mapHeight;
    /**
     * determines order of entity drawing
     */
    private Comparator<Entity> entityRenderOrderComparator;
    private CollisionTree collisionTree;
    private GridPoint2 dummyGridPoint = new GridPoint2();
    private Array<MapCell> dummyCellArray = new Array<>();
    private Array<Entity> dummyEntityArray = new Array<>();
    private Array<Entity> dummyEntitySet = new Array<>();
    private OrthographicCamera worldCamera;
    private Rectangle cameraRectangle = new Rectangle();
    private MyGame myGame;
    private int entityDeactivationBorder;
    private boolean deactivateEntities;
    private Rectangle deactivationRectangle = new Rectangle();
    private Array<MapCell> mapCellArray = new Array<>();
    private Notifier<Event> notifier = new Notifier<>();

    /**
     * @param entityDeactivationBorder    in world map units, border applied to each edge of the camera view to
     *                                    form the deactivation Rectangle. Entities outside that rectangle will not be updated
     * @param entityRenderOrderComparator what comparator to use, determines entity-drawing-order sorting; if null, entities
     *                                    aren't sorted before drawing
     **/
    public EntityTiledMap(OrthographicCamera worldCamera, boolean deactivateEntities, int entityDeactivationBorder,
                          Comparator<Entity> entityRenderOrderComparator, MyGame myGame) {
        this.worldCamera = worldCamera;
        this.entityRenderOrderComparator = entityRenderOrderComparator;
        this.myGame = myGame;
        this.entityDeactivationBorder = entityDeactivationBorder;
        this.deactivateEntities = deactivateEntities;
    }

    public void initMap(int mapWidth, int mapHeight, int collisionTreeCellSize) {
        mapCells = new ObjectMap<>();

        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                mapCells.put(new GridPoint2(x, y), new MapCell(x, y, 1, this));
            }
        }

        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;

        this.collisionTree = new CollisionTree(collisionTreeCellSize, mapWidth, mapHeight, this);
    }

    /**
     * Don't hold reference for the returned Array, as it is reused inside the object
     */
    public Array<MapCell> getMapCells() {
        mapCellArray.clear();
        return mapCells.values().toArray(mapCellArray);
    }

    /**
     * @return null if position is outside the map
     */
    public MapCell getMapCell(GridPoint2 position) {
        if (!isInsideMap(position.x, position.y))
            return null;

        return mapCells.get(position);
    }

    public MapCell getMapCell(int x, int y) {
        return getMapCell(dummyGridPoint.set(x, y));
    }

    public Array<Entity> getEntityList() {
        return entityList;
    }

    public final Notifier<Event> getBaseMapNotifier() {
        return notifier;
    }

    private void notifyEvent(Event event) {
        notifier.notifyObservers(event);
    }

    public CollisionTree getCollisionTree() {
        return collisionTree;
    }

    public void update(float delta) {
        updateCameraRectangle();
        removeAndAddEntities();

        collisionTree.resetDynamicEntities(dynamicEntityList);

        dummyEntitySet.clear();
        if (deactivateEntities)
            dummyEntitySet.addAll(collisionTree.getPossibleCollidingEntities(getDeactivationRectangle(), true));
        else
            dummyEntitySet.addAll(entityList);
        // update entities using culling
        for (Entity ent : dummyEntitySet) {
            ent.update(delta);

            if (!ent.isStatic() && ent.hasMoved()) {
                collisionTree.addDynamicEntityThatMoved(ent);
            }
        }
    }

    /**
     * Call outside of map.update() loop to update EntityList
     */
    public void removeAndAddEntities() {
        for (Entity ent : toBeRemoved) {
            entityList.removeValue(ent, true);

            if (ent.isStatic()) {
                collisionTree.removeStaticEntity(ent);
            } else {
                dynamicEntityList.removeValue(ent, true);
            }

            updateCells(ent, true);
        }

        for (Entity ent : toBeAdded) {
            entityList.add(ent);

            if (ent.isStatic()) {
                collisionTree.addStaticEntity(ent);
            } else {
                dynamicEntityList.add(ent);
            }

            updateCells(ent, false);
        }

        toBeRemoved.clear();
        toBeAdded.clear();
    }

    /**
     * Default implementation, override as needed
     */
    public void draw(GameDrawer gameDrawer, float delta) {
        drawCells(gameDrawer, delta);

        if (MyGame.DEBUG) {
            collisionTree.debugDrawCellBounds(gameDrawer);

            gameDrawer.setAlpha(0.3f);
            gameDrawer.setColor(Color.RED);
            gameDrawer.drawRectangle(cameraRectangle, 0.1f, false);
            gameDrawer.setColor(Color.BLUE);
            gameDrawer.drawRectangle(getDeactivationRectangle(), 0.1f, false);
            gameDrawer.resetColor();
            gameDrawer.resetAlpha();
        }

        drawEntities(gameDrawer, delta);
    }

    /**
     * Uses culling
     */
    protected void drawCells(GameDrawer gameDrawer, float delta) {
        Rectangle rectangle = Pools.obtain(Rectangle.class);
        for (MapCell cell : getCellsOccupiedByRectangle(getCameraRectangle(rectangle, drawingCameraBoundsBorder))) {
            cell.draw(gameDrawer, delta);

            // debug
            if (MyGame.DEBUG) {
                gameDrawer.setColor(Color.BLACK);
                gameDrawer.setAlpha(0.3f);
                gameDrawer.drawRectangle(cell.getX(), cell.getY(), 1, 1, 0.03f, false);

                gameDrawer.resetColor();
                gameDrawer.resetAlpha();
            }
        }

        Pools.free(rectangle);
    }

    private Rectangle getDeactivationRectangle() {
        getCameraRectangle(deactivationRectangle, entityDeactivationBorder);
        return deactivationRectangle;
    }

    /**
     * Returned camera rectangle is updated at the start of the map's update cycle, so be careful about posterior camera
     * modifications (use some border)
     *
     * @param border border around the camera, in game units; can be negative
     */
    public Rectangle getCameraRectangle(Rectangle rectangleToReturn, int border) {
        rectangleToReturn.set(cameraRectangle);

        if (border == 0) return rectangleToReturn;

        rectangleToReturn.x -= border;
        rectangleToReturn.y -= border;
        rectangleToReturn.width += border * 2;
        rectangleToReturn.height += border * 2;

        return rectangleToReturn;
    }

    private void updateCameraRectangle() {
        float width = worldCamera.viewportWidth * worldCamera.zoom;
        float height = worldCamera.viewportHeight * worldCamera.zoom;

        cameraRectangle.set((worldCamera.position.x - width / 2),
                (worldCamera.position.y - height / 2),
                width, height);
    }

    /**
     * uses culling
     */
    protected void drawEntities(GameDrawer gameDrawer, float delta) {
        dummyEntityArray.clear();

        Rectangle rectangle = Pools.obtain(Rectangle.class);
        for (Entity ent : collisionTree.getPossibleCollidingEntities(getCameraRectangle(rectangle, drawingCameraBoundsBorder),
                true)) {
            dummyEntityArray.add(ent);
        }

        if (entityRenderOrderComparator != null) {
            dummyEntityArray.sort(entityRenderOrderComparator);
        }

        for (Entity ent : dummyEntityArray) {
            ent.draw(gameDrawer, delta);
        }

        Pools.free(rectangle);
    }

    public MapCell getClosestValidCell(MapCell startingCell, boolean allowSolidCells, boolean allowCellsWithTiledEntity) {
        if (isCellValid(startingCell, allowSolidCells, allowCellsWithTiledEntity))
            return startingCell;

        int x = 0, y = 0, amount = 1, sign = 1;
        boolean yturn = false;

        // spiral loop around the start cell
        while (true) {
            // if cell is inside map
            int posx, posy;
            posx = startingCell.getX() + x;
            posy = startingCell.getY() + y;

            if (isInsideMap(posx, posy)) {
                MapCell cell = getMapCell(posx, posy);

                if (isCellValid(startingCell, allowSolidCells, allowCellsWithTiledEntity)) // is valid?
                {
                    return cell;
                }
            }

            // make a spiral loop
            if (yturn)
                y += sign;
            else
                x += sign;

            if (!yturn && x == sign * amount) {
                yturn = true;
            } else if (yturn && y == sign * amount) {
                yturn = false;
                sign *= -1;

                if (sign == 1)
                    amount++;
            }
        }
    }

    public boolean isCellValid(MapCell mapCell, boolean allowSolidCells, boolean allowCellsWithTiledEntity) {
        return ((allowSolidCells || mapCell.hasSolid()) && (allowCellsWithTiledEntity || mapCell.tiledEntities.size == 0));
    }

    public int fixPositionX(int x) {
        x = (int) Utils.clamp(x, 0, mapWidth - 1);

        return x;
    }

    public int fixPositionY(int y) {
        y = (int) Utils.clamp(y, 0, mapHeight - 1);

        return y;
    }

    public boolean isInsideMap(float x, float y) {
        return (x >= 0 && y >= 0 && x < mapWidth && y < mapHeight);
    }

    public void addEntity(Entity ent) {
        toBeAdded.add(ent);
    }

    public void removeEntity(Entity ent) {
        toBeRemoved.add(ent);
    }

    /**
     * update cell variables on cells affected by Entity's bounding box
     */
    public void updateCells(Entity ent, boolean removeEntity) {
        if (!ent.isStatic()) return;

        for (GridPoint2 cell : getCellsCoordsOccupiedByRectangle(ent.getBoundingBox())) {
            MapCell mapCell = getMapCell(cell.x, cell.y);

            // will ignore Static entities that overlap outside the map
            if (mapCell == null) {
                continue;

                /*throw new RuntimeException("Added Static Entity that overlaps a cell outside of map. \nEntity: " + ent
                        + "\nCell outside of map position: x: " + cell.x + ", y: " + cell.y);*/
            }

            if (ent.getsTiledToCell()) {
                if (removeEntity) {
                    mapCell.tiledEntities.removeValue(ent, true);
                } else {
                    mapCell.tiledEntities.add(ent);
                }

                if (MyGame.DEBUG)
                    mapCell.getFlashingThing().flashColor(Color.YELLOW, 0.6f, 0.1f);

                if (ent.isSolid()) {
                    mapCell.updateHasSolid();

                    if (MyGame.DEBUG)
                        mapCell.getFlashingThing().flashColor(Color.RED, 0.6f, 0.1f);
                }
            }
        }
    }

    /**
     * assumes that cell coordinates = units used in game
     */
    public Array<GridPoint2> getCellsCoordsOccupiedByRectangle(Rectangle rectangle) {
        GridPoint2 rectangleOrigin = Pools.obtain(GridPoint2.class);
        GridPoint2 rectangleEnd = Pools.obtain(GridPoint2.class);

        rectangleOrigin.set((int) Math.floor(rectangle.x), (int) Math.floor(rectangle.y));
        rectangleEnd.set((int) Math.floor(rectangle.x + rectangle.width), (int) Math.floor(rectangle.y + rectangle.height));

        Array<GridPoint2> cells = new Array<>();

        for (int xx = rectangleOrigin.x; xx <= rectangleEnd.x; xx++) {
            for (int yy = rectangleOrigin.y; yy <= rectangleEnd.y; yy++) {
                cells.add(new GridPoint2(xx, yy));
            }
        }

        Pools.free(rectangleEnd);
        Pools.free(rectangleOrigin);

        return cells;
    }

    public Array<MapCell> getCellsOccupiedByRectangle(Rectangle rectangle) {
        Array<MapCell> cells = dummyCellArray;
        cells.clear();

        for (GridPoint2 gridPoint2 : getCellsCoordsOccupiedByRectangle(rectangle)) {
            MapCell cell = getMapCell(gridPoint2.x, gridPoint2.y);
            if (cell != null)
                cells.add(cell);
        }

        return cells;
    }

    public int getWidth() {
        return mapWidth;
    }

    public int getHeight() {
        return mapHeight;
    }

    public void dispose() {

    }

    public MyGame getMyGame() {
        return myGame;
    }

    public OrthographicCamera getWorldCamera() {
        return worldCamera;
    }

    public enum Event {
        MAP_SOLID_INFORMATION_CHANGED

    }
}
