package com.libgdx.my_game.engine.map;

import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.utils.ObjectSet;
import com.libgdx.my_game.engine.map.entities.Entity;

/**
 * Created by Darius on 20/10/2017.
 */
public class CollisionTreeCell {
    public ObjectSet<Entity> entities = new ObjectSet<>();
    public GridPoint2 position;
    private boolean isOutside;

    CollisionTreeCell(GridPoint2 position, boolean isOutside) {
        this.position = position;
        this.isOutside = isOutside;
    }

    boolean isOutsideOfMap() {
        return isOutside;
    }
}
