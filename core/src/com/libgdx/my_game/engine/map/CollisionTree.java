package com.libgdx.my_game.engine.map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.StringBuilder;
import com.libgdx.my_game.utils.GameDrawer;
import com.libgdx.my_game.MyGame;
import com.libgdx.my_game.engine.map.entities.Entity;

/**
 * Created by Darius on 20/10/2017.
 * <p>
 * Holds entities in cells, each one with configurable size on variable SIZE
 * Cells occupy the entire map size provided, creating as many cells as necessary to fill the map
 * <p/>
 * Holds two types of entities, dynamic and static
 * Dynamic entities are added on each step. Before that method resetDynamicEntities() must be called
 * Static entities are added on creation, and removed when destroyed
 * Dynamic entities can move, Static entities must have flag <isStatic> activated. Assumes they will never move!
 * <p/>
 * All coordinates not related to arrays of cells are in game units - assumes game map cells are size of 1 unit each!
 */
public class CollisionTree {
    private int SIZE;
    private int sizeX;
    private int sizeY;

    private CollisionTreeCell[][] treeCells;
    private CollisionTreeCell outsideCell;

    private Array<Entity> dynamicEntities = new Array<>();
    private Array<Entity> dynamicEntitiesThatMoved = new Array<>();

    private com.libgdx.my_game.engine.map.EntityTiledMap entityTiledMap;

    private Vector2 dummyVector2 = new Vector2();
    private GridPoint2 dummyGridPoint1 = new GridPoint2();
    private GridPoint2 dummyGridPoint2 = new GridPoint2();
    private Array<CollisionTreeCell> dummyTreeCellSet = new Array<>();
    private Array<Entity> dummyEntityArray = new Array<>();
    private Array<Entity> dummyEntitySet = new Array<>();
    private Array<CollisionTreeCell> dummyTreeCellArray = new Array<>();

    private ObjectMap<Entity, Array<CollisionTreeCell>> dynamicEntitiesPosition = new ObjectMap<>();

    private GridPoint2 rectangleOrigin = new GridPoint2();
    private GridPoint2 rectangleEnd = new GridPoint2();

    private int cellNumber = 0;

    CollisionTree(int SIZE, int mapSizeX, int mapSizeY, com.libgdx.my_game.engine.map.EntityTiledMap entityTiledMap) {
        this.SIZE = SIZE;

        sizeX = (int) Math.ceil((double) mapSizeX / (double) SIZE);
        sizeY = (int) Math.ceil((double) mapSizeY / (double) SIZE);

        treeCells = new CollisionTreeCell[sizeX][sizeY];
        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                treeCells[x][y] = new CollisionTreeCell(new GridPoint2(x, y), false);
                cellNumber++;
            }
        }

        outsideCell = new CollisionTreeCell(new GridPoint2(-1, -1), true);

        this.entityTiledMap = entityTiledMap;
    }

    private boolean isTreeCellPositionInsideTree(int x, int y) {
        return (x >= 0 && y >= 0 && x < sizeX && y < sizeY);
    }

    /**
     * Returns list of Entities surrounding input Entity, based on input maxDistance
     * <p/>
     * Checks adjacent treeCells following a spiral, minimum check is a complete spiral around the first treeCell.
     * Stops searching when current spiral of cell has a maximum estimated distance >= maxDistance
     * (but includes that spiral of cells).
     * <p/>
     * Method useful when searching for closest entities. Use another method for collision-checking.
     * <p/>
     * Ignores Entities outside of the map, and returns nothing if input Entity is outside.
     * <p/>
     *
     * @return A list of entities ordered from closest to farthest
     * It's an estimation, so that first entity in the list could be farther from the third
     * But assures that in large search areas last entities are farthest, so if you loop through the array you start checking
     * the (possibly) closest ones. Also being allowed to specify the maximum distance, the array is limited in size
     * <p/>
     * Warning: Don't keep references to the returned Array, as it is reused in this object
     */
    public Array<Entity> getClosestEntities(Entity entity, int maxDistance) {
        entity.getTiledPosition(dummyGridPoint2);
        getTreeCellCoordsFromMapCellCoords(dummyGridPoint2);
        CollisionTreeCell startingCell = getTreeCellFromTreeCellCoords(dummyGridPoint2.x, dummyGridPoint2.y);

        boolean exit = false;
        int count = 0;
        Array<Entity> entities = dummyEntityArray;
        entities.clear();

        // input entity is outside
        if (startingCell == outsideCell) return entities;

        GridPoint2 position = dummyGridPoint1;
        int x = 0, y = 0, amount = 1, sign = 1;
        boolean yTurn = false;

        // spiral loop around the start cell
        while (true) {
            position.set(startingCell.position.x + x, startingCell.position.y + y);

            // stop when you checked all cells
            if (count == cellNumber) {
                exit = true;
            } else if (isTreeCellPositionInsideTree(position.x, position.y)) {
                count++;

                // stop when the outer ring of cells is farther that the maxDistance
                // but, include that ring. Also always include the first ring around the starting cell
                int cycle = Math.max(Math.abs(x), Math.abs(y));
                if ((cycle - 1) * SIZE > maxDistance && cycle != 0)
                    exit = true;

                for (Entity ent : treeCells[position.x][position.y].entities) {
                    entities.add(ent);
                }
            }

            if (exit) {
                return entities;
            }

            // make a spiral loop
            if (yTurn)
                y += sign;
            else
                x += sign;

            if (!yTurn && x == sign * amount) {
                yTurn = true;
            } else if (yTurn && y == sign * amount) {
                yTurn = false;
                sign *= -1;

                if (sign == 1)
                    amount++;
            }
        }
    }

    /**
     * Don't keep references to the returned Array, as it is reused in this object <p />
     * Includes Entities outside of the map (if the input boundingBox is outside of the map)
     *
     * @param includeDynamicEntities If true, forces an update of dynamic Entities positions
     */
    public Array<Entity> getPossibleCollidingEntities(Rectangle boundingBox, boolean includeDynamicEntities) {
        if (includeDynamicEntities)
            updateDynamicEntitiesThatMoved();

        Array<Entity> entities = dummyEntityArray;
        entities.clear();

        getEntitiesFromTreeCellsOccupiedByBoundingBox(boundingBox, entities, !includeDynamicEntities, false);

        return entities;
    }

    /** modifies input Array */
    private void getEntitiesFromTreeCellsOccupiedByBoundingBox(Rectangle boundingBox, Array<Entity> entityArrayToPopulate,
                                                               boolean excludeDynamicEntities, boolean excludeStaticEntities)
    {
        for (CollisionTreeCell treeCell : getTreeCellsOverlappedByRectangle(boundingBox)) {
            for (Entity ent : treeCell.entities) {
                if ((excludeDynamicEntities && !ent.isStatic()) || (excludeStaticEntities && ent.isStatic())) {
                    continue;
                }

                // avoid duplicates
                if (entityArrayToPopulate.contains(ent, true)) continue;

                if (MyGame.DEBUG && boundingBox.area() < 10) { // try to ignore camera checks for debug purposes
                    ent.getDebugEntityFlashingThing().flashColor(Color.RED, 0.3f);
                }

                entityArrayToPopulate.add(ent);
            }
        }
    }

    public void addDynamicEntityThatMoved(Entity entity) {
        dynamicEntitiesThatMoved.add(entity);
    }

    /**
     * Use to update dynamic entities that moved, to get correct collision information as Dynamic Entities can move anytime
     */
    private void updateDynamicEntitiesThatMoved() {
        for (Entity entity : dynamicEntitiesThatMoved) {
            addEntity(entity, true, true);
            addEntity(entity, true, false);
        }
    }

    private void addEntity(Entity ent, boolean isDynamic, boolean remove) {
        dummyTreeCellArray.clear();

        if (isDynamic && remove) {
            for (CollisionTreeCell cell : dynamicEntitiesPosition.get(ent)) {
                cell.entities.remove(ent);
            }

            dynamicEntitiesPosition.remove(ent);
            return;
        }

        // find out which cells the Entity occupies
        Array<CollisionTreeCell> cells = getTreeCellsOverlappedByRectangle(ent.getBoundingBox());
        for (CollisionTreeCell treeCell : cells) {
            if (!remove) {
                treeCell.entities.add(ent);
            } else {
                treeCell.entities.remove(ent);
            }
        }

        // only on add
        if (isDynamic) {
            dynamicEntitiesPosition.put(ent, new Array<>(cells));
        }
    }

    /**
     * Don't keep references to the returned Array nor its contents, as they are reused in this object
     *
     * @param rectangle Positioned and sized in game map units
     */
    public Array<CollisionTreeCell> getTreeCellsOverlappedByRectangle(Rectangle rectangle) {
        Array<CollisionTreeCell> returnedCells = dummyTreeCellSet;
        returnedCells.clear();

        rectangleOrigin.set((int) (rectangle.x), (int) (rectangle.y));
        rectangleEnd.set((int) (rectangle.x + rectangle.width), (int) (rectangle.y + rectangle.height));

        GridPoint2 cell1 = getTreeCellCoordsFromMapCellCoords(rectangleOrigin);
        GridPoint2 cell2 = getTreeCellCoordsFromMapCellCoords(rectangleEnd);

        for (int x = cell1.x; x <= cell2.x; x++) {
            for (int y = cell1.y; y <= cell2.y; y++) {
                returnedCells.add(getTreeCellFromTreeCellCoords(x, y));
            }
        }

        return returnedCells;
    }

    /**
     * Modifies input vector
     */
    private GridPoint2 getTreeCellCoordsFromMapCellCoords(GridPoint2 mapCellCoords) {
        // outside coords are all transformed to -1, -1 or sizeX, sizeY (first outside coordinate on all sides)
        // this avoids unnecessary loops if looping over all range of coords
        return mapCellCoords.set(Math.min(Math.max(-1, (int) (mapCellCoords.x / (double) SIZE)), sizeX),
                Math.min(Math.max(-1, (int) (mapCellCoords.y / (double) SIZE)), sizeY));
    }

    private CollisionTreeCell getTreeCellFromTreeCellCoords(int treeCellX, int treeCellY) {
        if (isTreeCellPositionInsideTree(treeCellX, treeCellY)) {
            return treeCells[treeCellX][treeCellY];
        } else {
            return outsideCell;
        }
    }

    public void resetDynamicEntities(Array<Entity> updatedDynamicEntityList) {
        for (Entity entity : dynamicEntitiesPosition.keys()) {
            if (!entity.hasMoved() && !entity.isKilled()) continue;

            entity.resetHasMoved();
            addEntity(entity, true, true);
        }

        for (Entity ent : updatedDynamicEntityList) {
            if (ent.hasMoved() || !dynamicEntitiesPosition.containsKey(ent)) {
                addEntity(ent, true, false);
            }
        }

        dynamicEntitiesThatMoved.clear();
    }

    void addStaticEntity(Entity ent) {
        addEntity(ent, false, false);
    }

    void removeStaticEntity(Entity ent) {
        addEntity(ent, false, true);
    }

    public String getDebugInfo() {
        StringBuilder string = new StringBuilder();
        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                StringBuilder stringBuilder = new StringBuilder();
                ObjectMap<Class, Integer> h = new ObjectMap<>();
                for (Entity e : treeCells[x][y].entities) {
                    h.put(e.getClass(), h.get(e.getClass(), 0) + 1);
                }

                for (ObjectMap.Entry<Class, Integer> entry : h.entries()) {
                    stringBuilder.append("\t").append(entry.key.getSimpleName()).append(" x ")
                            .append(String.valueOf(entry.value)).append("\n");
                }


                string.append("quadCell x: ").append(String.valueOf(x)).append(", y: ").append(String.valueOf(y))
                        .append("\nentities: \n").append(stringBuilder.toString()).append("\n");

            }
        }

        return string.toString();
    }

    public void debugDrawCellBounds(GameDrawer gameDrawer) {
        gameDrawer.setColor(Color.RED);
        gameDrawer.setAlpha(0.09f);

        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                gameDrawer.drawRectangle(x * SIZE, y * SIZE, SIZE, SIZE, 0.06f, false);
            }
        }

        gameDrawer.resetColor();
        gameDrawer.resetAlpha();
    }
}
