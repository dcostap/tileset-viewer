package com.libgdx.my_game.engine.map.map_loading;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;
import com.libgdx.my_game.MyGame;
import com.libgdx.my_game.engine.map.entities.Entity;
import com.libgdx.my_game.engine.map.EntityTiledMap;
import com.libgdx.my_game.utils.Utils;

import java.io.File;


/**
 * Handles the loading of objects and tiles inside a map created with Tiled map editor, exported to .json files.
 * <ul>
 * Uses:
 * <li>.json file of the map itself</li>
 * <li>.json file of each tileSet the map uses</li>
 * <li>.json file of each object template the map uses</li>
 * </ul>
 * <ul>
 * Loads:
 * <li>TileLayers: loads the tile images into the game map cells</li>
 * <li>ObjectLayers: creates Entities for each Object. Can be Tiled's tile objects or shaped objects.
 * Tile objects load Entities based on its tile image name; shaped objects load Entities based on each object name.
 * See {@link com.libgdx.my_game.engine.map.map_loading.EntityLoaderFromString}</li>
 * </ul>
 * <ul>
 * Notes:
 * <li>While working on Tiled, base files may be different than .json (.tmx for example). You must then use "export"
 * option and export them as .json each time you edit. You may use .json base file for convenience. (When creating tileSets or
 * templates, save them as .json when first prompted)</li>
 * <li>Size of objects in Tiled may or may not be ignored when loading them into Entities</li>
 * <li>Note that when using object templates, the size change in one instance of that template won't be saved, so in the game
 * the loaded size is the template's object size. To save one template's instance size, detach it from the template</li>
 * </ul>
 */
public class JsonMapLoader implements com.libgdx.my_game.engine.map.map_loading.MapLoader {

    /**
     * TilesetLoaders are stored here, one for each tileset
     * TilesetLoaders are only created when a tileset is first needed while reading the map
     */
    private ObjectMap<String, com.libgdx.my_game.engine.map.map_loading.JsonTileSet> loadedTileSets;

    private JsonReader jsonReader;
    private String mapJsonLocation;
    private String tileSetsJsonLocation;
    private String objectTemplatesJsonFolder;
    private EntityTiledMap map;
    private com.libgdx.my_game.engine.map.map_loading.EntityLoaderFromString entityLoaderFromString;

    public JsonMapLoader(EntityTiledMap map, String mapJsonFilePath, String tileSetsJsonFolder,
                         String objectTemplatesJsonFolder, com.libgdx.my_game.engine.map.map_loading.EntityLoaderFromString entityLoaderFromString) {
        this.map = map;
        this.mapJsonLocation = mapJsonFilePath;
        this.tileSetsJsonLocation = tileSetsJsonFolder;
        this.objectTemplatesJsonFolder = objectTemplatesJsonFolder;
        this.entityLoaderFromString = entityLoaderFromString;

        loadedTileSets = new ObjectMap<>();
        jsonReader = new JsonReader();
    }

    @Override
    public void loadMap() {
        JsonValue jsonMapFile = jsonReader.parse(Gdx.files.internal(mapJsonLocation));

        int height = jsonMapFile.get("height").asInt();
        int width = jsonMapFile.get("width").asInt();

        map.initMap(width, height, 8);

        loadTileLayers(jsonMapFile, map);
        loadObjectLayers(jsonMapFile, map);
    }

    /**
     * Loads the info inside all tile layers found in the .json file.
     */
    private void loadTileLayers(JsonValue jsonMapFile, EntityTiledMap map) {
        // iterate through all tile layers of the map
        JsonValue layers = jsonMapFile.get("layers");
        for (JsonValue layer = layers.child; layer != null; layer = layer.next) {
            if (layer.getString("type").equals("tilelayer")) {
                loadTileLayer(jsonMapFile, layer, map);
            }
        }
    }

    /**
     * Loops through all cells inside the tile layer. Then calls EntityLoaderFromString to load an Entity based on each cell's
     * linked texture inside the tileset the cell is using.
     */
    private void loadTileLayer(JsonValue jsonMapFile, JsonValue jsonLayerInfo, EntityTiledMap map) {
        int width = map.getWidth();
        int height = map.getHeight();

        GridPoint2 cellPosition = new GridPoint2();

        // loop through all the cells of the layer
        int[] cells = jsonLayerInfo.get("data").asIntArray();
        for (int i = 0; i < cells.length; i++) {
            // GID of the cell = global identifier that links the cell with a texture inside a tileset
            int GID = cells[i];

            // GID = 0 means cell has no texture
            if (GID == 0) continue;

            cellPosition.set(i % width, height - i / width - 1);

            String imageName = getTileImageNameFromGIDInsideTileSet(GID, jsonMapFile, null);

            loadTileIntoMap(imageName, cellPosition, map);
        }
    }

    /**
     * Modifies the MapCell in the specified position to have a tile image based on tileImageFile
     */
    private void loadTileIntoMap(String tileImageFile, GridPoint2 cellPosition, EntityTiledMap map) {
        map.getMapCell(cellPosition).getTile().rotation = 0;
        map.getMapCell(cellPosition).getTile().tileSpr = map.getMyGame().getAssets().findRegionFromRawImageName(tileImageFile);
    }

    private void loadObjectLayers(JsonValue jsonMapFile, EntityTiledMap map) {
        // iterate through all object layers of the map
        JsonValue layers = jsonMapFile.get("layers");
        for (JsonValue layer = layers.child; layer != null; layer = layer.next) {
            if (layer.getString("type").equals("objectgroup")) {
                loadObjectLayer(jsonMapFile, layer, map);
            }
        }
    }

    private void loadObjectLayer(JsonValue jsonMapFile, JsonValue jsonLayerInfo, EntityTiledMap map) {
        // loop through all objects
        JsonValue objects = jsonLayerInfo.get("objects");
        for (JsonValue objectInfo = objects.child; objectInfo != null; objectInfo = objectInfo.next) {
            Vector2 position = new Vector2(objectInfo.getInt("x"), objectInfo.getInt("y"));

            // Tiled saves object's coords as pixel units with origin on top-left, so translate it to game coords
            position.x /= (float) MyGame.PPM;
            position.y /= (float) MyGame.PPM;
            position.y = map.getHeight() - position.y;

            boolean isTileObject;

            // if it's a template, the actual info will be located in that template file
            JsonValue objectActualInfo = objectInfo;

            // used to store the template info root when the object is a template object, to be used in methods
            // to retrieve tileset info - it's in the root of the template file (and objectActualInfo is in a child of the root)
            JsonValue templateInfoRootJson = null;

            // find if it's a tile object -> needs to find if it has a "gid" attribute in the .json
            // if it's a template object, the attribute is in the template file, so parse that file
            if (objectInfo.get("template") == null) {
                isTileObject = objectActualInfo.get("gid") != null;
            } else {
                String templateFile = objectInfo.getString("template");
                File tileset = new File(templateFile);
                templateFile = tileset.getName(); // using File's getName() path modifiers are removed

                // parse .json template file
                objectActualInfo = jsonReader.parse(Gdx.files.internal(objectTemplatesJsonFolder + File.separator + templateFile));
                templateInfoRootJson = objectActualInfo;
                objectActualInfo = objectActualInfo.get("object");

                isTileObject = objectActualInfo.get("gid") != null;
            }

            int widthPixels = objectActualInfo.getInt("width");
            int heightPixels = objectActualInfo.getInt("height");

            // is tile object?
            if (isTileObject) {
                loadTileObject(jsonMapFile, map, position, widthPixels, heightPixels, objectActualInfo, templateInfoRootJson);
            } else {
                // Tiled objects have their origin on their top-left corner, fix that. (the origin is "right" in tileObjects)
                position.y -= (float) heightPixels / MyGame.PPM;

                loadObject(map, position, widthPixels, heightPixels, objectActualInfo);
            }
        }
    }

    private void loadTileObject(JsonValue jsonMapFile, EntityTiledMap map, Vector2 position, int widthPixels, int heightPixels,
                                JsonValue objectInfo, JsonValue templateInfoRootJson) {
        int GID = objectInfo.getInt("gid");
        if (GID == 0) return;

        String tileImageName = getTileImageNameFromGIDInsideTileSet(GID, jsonMapFile, templateInfoRootJson);
        Entity entity = entityLoaderFromString.loadEntityFromTiledTileObject(tileImageName, position, widthPixels, heightPixels, map);
        map.addEntity(entity);
    }

    /**
     * @param objectInfo Object's name as configured in Tiled and saved in .json. If saved as a template object, the
     *                   name will be in the template file
     */
    private void loadObject(EntityTiledMap map, Vector2 position, int widthPixels, int heightPixels,
                            JsonValue objectInfo) {
        String objectName = objectInfo.getString("name");
        Entity entity = entityLoaderFromString.loadEntityFromObjectName(objectName, position, widthPixels, heightPixels, map);
        map.addEntity(entity);
    }

    /**
     * Returns the image name of the tile with the GID provided
     * Looks in the folder where tileSets are for the tileSet where that GID belongs
     *
     * @param objectTemplateInfoRootJson The .json info of the template file, if it's a template object
     *                                   If not an object (tile), pass null
     * @return Name of the image, without extension and path modifiers
     */
    private String getTileImageNameFromGIDInsideTileSet(int GID, JsonValue jsonMapFile, JsonValue objectTemplateInfoRootJson) {
        JsonValue jsonTileSet = null;

        // find the tileset: if it's a object from a template, the tileset is in the template file
        // if not, the tileset is in one of the tileset array in the jsonMapFile
        if (objectTemplateInfoRootJson == null || objectTemplateInfoRootJson.get("tileset") == null) {
            JsonValue jsonTileSetsInfo = jsonMapFile.get("tilesets");

            // iterate through all the tileSets of the map
            // using the "firstGID" property of each tileset, find the tileset that belongs to the GID provided
            for (JsonValue entry = jsonTileSetsInfo.child; entry != null; entry = entry.next) {
                if (GID >= entry.getInt("firstgid")) {
                    jsonTileSet = entry;
                }
            }
        } else {
            // template
            jsonTileSet = objectTemplateInfoRootJson.get("tileset");
        }

        // get the name of the tileset file, without extension
        String tilesetFile = jsonTileSet.getString("source");
        File tileset = new File(tilesetFile);
        String fileName = tileset.getName(); // using File's getName() path modifiers are removed
        tilesetFile = Utils.removeExtensionFromFilename(fileName); // remove extension

        // if it's the first time this tileset is accessed, create a JsonTileSet
        if (!loadedTileSets.containsKey(tilesetFile)) {
            com.libgdx.my_game.engine.map.map_loading.JsonTileSet tilesetLoader = new com.libgdx.my_game.engine.map.map_loading.JsonTileSet(buildTilesetFileName(tilesetFile), jsonReader);
            loadedTileSets.put(tilesetFile, tilesetLoader);
        }

        // get the local tileset image ID from the Global ID (GID) of the cell
        int localTileID = GID - jsonTileSet.getInt("firstgid");

        return loadedTileSets.get(tilesetFile).getImageNameFromTileId(localTileID);
    }

    private String buildTilesetFileName(String tileset_name) {
        return tileSetsJsonLocation + File.separator + tileset_name + ".json";
    }
}
