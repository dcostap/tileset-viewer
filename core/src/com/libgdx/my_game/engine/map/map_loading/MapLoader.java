package com.libgdx.my_game.engine.map.map_loading;

/**
 * Created by Darius on 14/01/2018
 */
public interface MapLoader {
    void loadMap();
}
