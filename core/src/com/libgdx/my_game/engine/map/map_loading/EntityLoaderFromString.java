package com.libgdx.my_game.engine.map.map_loading;

import com.badlogic.gdx.math.Vector2;
import com.libgdx.my_game.engine.map.entities.Entity;
import com.libgdx.my_game.engine.map.EntityTiledMap;

/**
 * Links strings with Entity creation
 */
public interface EntityLoaderFromString {
    /**
     * Loads Entity associated with a Tiled's TileObject (Object with image). Identified by its tile image name
     *
     * @param imageName    The name of the tile used by the Tile Object
     * @param heightPixels Depending on object created, may be used or ignored; same with width
     */
    Entity loadEntityFromTiledTileObject(String imageName, Vector2 position, int widthPixels, int heightPixels, EntityTiledMap map);

    /**
     * Load Entity associated with a Tiled's Object (not a TileObject). Identified by its name
     */
    Entity loadEntityFromObjectName(String objectName, Vector2 position, int widthPixels, int heightPixels, EntityTiledMap map);
}
