package com.libgdx.my_game.engine.map.map_loading;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.libgdx.my_game.utils.Utils;

import java.io.File;

/**
 * Created by Darius on 14/11/2017
 * <p>
 * Loads one tileSet from the provided path
 * Provides methods to retrieve an Image inside the tileSet from the provided local ID
 */
class JsonTileSet {
    private JsonReader jsonReader;
    private JsonValue tileSet;

    JsonTileSet(String tileSetFilePath, JsonReader jsonReader) {
        this.jsonReader = jsonReader;

        loadTileSet(tileSetFilePath);
    }

    private void loadTileSet(String tileSetFilePath) {
        tileSet = jsonReader.parse(Gdx.files.internal(tileSetFilePath));
    }

    /**
     * Retrieves the image name from the image with provided ID inside the tileSet
     *
     * @param localTileID local ID of the image inside the tileSet, not to confuse with GID
     *                    (global ID in the Tiled map, across all tileSets)
     * @return name of the image, without extension
     */
    String getImageNameFromTileId(int localTileID) {
        JsonValue tile = tileSet.get("tiles").get(Integer.toString(localTileID));
        String image_filename = tile.get("image").asString();
        int height = tile.get("imageheight").asInt();
        int width = tile.get("imagewidth").asInt();
        File image = new File(image_filename);
        return Utils.removeExtensionFromFilename(image.getName());
    }
}
