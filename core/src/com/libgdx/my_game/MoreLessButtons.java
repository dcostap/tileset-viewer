package com.libgdx.my_game;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.libgdx.my_game.engine.hud.utils.LabelButton;

/**
 * Created by Darius on 10/03/2018.
 */
public abstract class MoreLessButtons extends Table {
    private Button moar, less;

    public MoreLessButtons(Skin skin, BitmapFont font, float buttonSize) {
        super();

        moar = new LabelButton("+", font, skin);
        less = new LabelButton("-", font, skin);

        add(moar).size(buttonSize);
        row();
        add(less).size(buttonSize);

        moar.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                moreAction();
            }
        });

        less.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                lessAction();
            }
        });
    }

    public MoreLessButtons(Skin skin, BitmapFont font) {
        this(skin, font, 20);
    }

    public abstract void moreAction();
    public abstract void lessAction();
}
