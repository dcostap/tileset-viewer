package com.libgdx.my_game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.libgdx.my_game.utils.Utils;

public class MyGame extends Game {
    /** Pixels per game unit **/
    public static final int PPM = 32;

    public static final int BASE_HEIGHT = 450;
    public static final int BASE_WIDTH = 800;

    public static boolean DEBUG = false;

    private static float distanceFactor = 1;
    private float smoothedDelta = 1 / 60f;
    private SpriteBatch batch;
    private Assets assets;

    public float fixedDelta = 1 / 60f;

    /** If true, it will ignore the app's delta */
    public boolean useFixedDelta = true;

    public static float getDensityFactor() {
        return Gdx.graphics.getDensity() * distanceFactor;
    }

    public static float pixelsToUnits(float pixels) {
        return pixels / PPM;
    }

    public static float unitsToPixels(float units) {
        return units * PPM;
    }

    /** Use to load the distanceFactor, once this class is loaded (otherwise calling Gdx.app.etc might crash the app). <p />
     * This allows desktop density factor to be bigger, since eyes will be further away from screen than in a mobile device, it
     * is needed to increase the density factor by a bit. So it is multiplied by distance factor (default: 1.5) */
    private static float getDistanceFactor() {
        switch (Gdx.app.getType()) {
            case Desktop:
                return 1.5f;
            default:
                return 1;
        }
    }

    public SpriteBatch getBatch() {
        return batch;
    }

    public Assets getAssets() {
        return assets;
    }

    @Override
    public void create() {
        MyGame.distanceFactor = getDistanceFactor();

        batch = new SpriteBatch();

        assets = new Assets();
        assets.initAssetLoading();

        // wait for assets to load
        while (!assets.finishAssetLoading()) {

        }

        this.setScreen(new TilesetViewerScreen(this));
    }

    @Override
    public void render() {
        // limit delta value
        float delta = Gdx.graphics.getDeltaTime();
        delta = Utils.clamp(delta, delta, 1 / 25f);

        // smooth delta to avoid wonky movement at high speeds
        float smoothIncrement = 1 / 1000f;
        if (smoothedDelta < delta) {
            smoothedDelta = Math.min(delta, smoothedDelta + smoothIncrement);
        } else if (smoothedDelta > delta) {
            smoothedDelta = Math.max(delta, smoothedDelta - smoothIncrement);
        }

        if (screen != null) screen.render(useFixedDelta ? fixedDelta : smoothedDelta);
    }

    @Override
    public void dispose() {
        assets.dispose();
        batch.dispose();
    }
}
