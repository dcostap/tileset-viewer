package com.libgdx.my_game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.libgdx.my_game.engine.hud.utils.ExtendedLabel;
import com.libgdx.my_game.utils.Utils;
import com.libgdx.my_game.utils.font_loaders.FontSetNormal;
import com.libgdx.my_game.utils.font_loaders.FontSetSingle;
import com.libgdx.my_game.utils.font_loaders.FreeTypeFontLoader;

/**
 * Created by Darius on 11/11/2017.
 */
public class Assets implements Disposable {
    private final String textureAtlasFilename = "atlas/atlas.atlas";
    private final String ttpFontFolder = "fonts/true_type";
    private final String bitmapFontFolder = "fonts/bitmap";

    public FontSetNormal fontDefault = new FontSetNormal();
    public FontSetSingle fontOutline = new FontSetSingle();

    public Skin skin;
    private TextureAtlas textureAtlas;
    private AssetManager assetManager;

    private boolean firstTimeLoadingFonts = true;
    private float initialTimeLoadingFonts = 0;

    private Array<FreeTypeFontLoader> fontLoaders = new Array<>();

    public FileHandle loadFile(String file) {
        return Gdx.files.internal(file);
    }

    /** Call first to setup the Asset Manager loading.
     * After you can use the Asset Manager to load the assets in another thread */
    public void initAssetLoading() {
        assetManager = new AssetManager();
        assetManager.load(textureAtlasFilename, TextureAtlas.class);

        loadFontsToAssetManager();
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    /** call before finishing loading assetManager */
    private void loadFontsToAssetManager() {
        // create the font loaders
        fontLoaders.add(new FreeTypeFontLoader(ttpFontFolder, "Roboto-Regular.ttf",
                "font1", fontDefault, false, 0, null));

        FreeTypeFontGenerator.FreeTypeFontParameter parameter =
                FreeTypeFontLoader.getGeneratorParameterFromConfig(Color.WHITE, 0.1f, Color.BLACK);
        fontLoaders.add(new FreeTypeFontLoader(ttpFontFolder, "Roboto-Regular.ttf",
                "font2", fontOutline, false, 0, parameter));

        // load fonts from asset manager
        for (FreeTypeFontLoader fontLoader : fontLoaders) {
            fontLoader.loadFonts(assetManager);
        }
    }

    /** Finishes both Asset Manager and own Assets loading
     * @return whether it finished */
    public boolean finishAssetLoading() {
        if (assetManager.update()) {
            if (!finishLoadingFonts()) return false;

            this.textureAtlas = assetManager.get(textureAtlasFilename, TextureAtlas.class);

            skin = new Skin();

            skin.addRegions(new TextureAtlas(Gdx.files.internal("skins/finished/skin.atlas")));
            skin.load(Gdx.files.internal("skins/finished/skin.json"));

            // manually add Window default style; since fonts are generated and Window needs a font to be usable, this can't
            // be done on SkinComposer
            skin.add("default", new Window.WindowStyle(fontDefault.font_small, Color.BLACK, skin.getDrawable("outline2_pixel")));
            skin.add("nonPixel", new Window.WindowStyle(fontDefault.font_small, Color.BLACK, skin.getDrawable("outline2")));
            skin.add("default", new TextField.TextFieldStyle(fontDefault.font_small, Color.BLACK,
                    skin.getDrawable("cursor"), skin.getDrawable("cursor"), skin.getDrawable("outline2")));
            return true;
        }

        return false;
    }

    /** call after finishing loading assetManager
     * Will load one font at a time, so the program doesn't hang for too long. (Causes problems on Android)
     * @return true when it finished*/
    private boolean finishLoadingFonts() {
        if (firstTimeLoadingFonts) {
            firstTimeLoadingFonts = false;
            initialTimeLoadingFonts = System.currentTimeMillis();
            System.out.println("____\nStarted loading fonts");

        }

        if (fontLoaders.size > 0) {
            fontLoaders.get(0).finishLoading(assetManager);
            fontLoaders.removeIndex(0);
            return false;
        } else {
            System.out.println("Finished loading fonts; elapsed: "
                    + ((System.currentTimeMillis() - initialTimeLoadingFonts) / 1000f) + "s\n____");
            return true;
        }
    }

    /**
     * Finds image in the atlas ignoring extensions. If the name ends with "_#" it loads it from the array that texturePacker creates
     */
    public TextureRegion findRegionFromRawImageName(String rawImageName) {
        Utils.removeExtensionFromFilename(rawImageName);

        int i = rawImageName.lastIndexOf('_');
        if (i != -1) {
            int index = Integer.valueOf(rawImageName.substring(i + 1));
            String realName = rawImageName.substring(0, i);
            return getTextureAtlas().findRegion(realName, index);
        } else {
            return findRegion(rawImageName);
        }
    }

    public TextureAtlas getTextureAtlas() {
        return textureAtlas;
    }

    public TextureAtlas.AtlasRegion findRegion(String name) {
        TextureAtlas.AtlasRegion atlasRegion = getTextureAtlas().findRegion(name);
        if (atlasRegion == null) throw new RuntimeException("Region not found on atlas, name: " + name);
        return atlasRegion;
    }

    public Array<TextureAtlas.AtlasRegion> findRegions(String name) {
        Array<TextureAtlas.AtlasRegion> atlasRegions = textureAtlas.findRegions(name);
        if (atlasRegions.size == 0) throw new RuntimeException("Group of regions ( .findRegions() ) not found on atlas, name: " + name);
        return atlasRegions;
    }

    @Override
    public void dispose() {
        assetManager.dispose();
        skin.dispose();
    }

    /**
     * Convenience method to add a Label style to the skin's json
     * Automatically creates the style based on a {@link BitmapFont}, with its color
     * @deprecated no need to use LabelStyles anymore, use {@link ExtendedLabel} instead
     */
    private void addLabelStyleToJsonSkin(String styleName, BitmapFont font) {
        skin.add(styleName, new Label.LabelStyle(font, font.getColor()), Label.LabelStyle.class);
    }
}
