package com.libgdx.my_game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.file.FileChooser;
import com.kotcrab.vis.ui.widget.file.FileChooserAdapter;
import com.libgdx.my_game.engine.hud.utils.ExtendedLabel;
import com.libgdx.my_game.engine.hud.utils.LabelButton;
import com.libgdx.my_game.utils.GameDrawer;
import com.libgdx.my_game.utils.Utils;
import com.libgdx.my_game.utils.screens.BaseScreenWithHud;

/**
 * Created by Darius on 07/03/2018.
 */
public class TilesetViewerScreen extends BaseScreenWithHud {


    ScrollPane tilesPane;
    ScrollPane loadedImagesPane;
    TextField pathField;
    Button confirmPath;
    Table idsTable;

    Array<Texture> loadedTextures;
    ObjectMap<String, Integer> imagesAndId;
    ObjectMap<Texture, String> textureToName;

    Array<Actor> destroyableActors = new Array<>();

    int tileWidth = 16;
    int tileHeight = 16;
    int scale = 4;
    boolean seeIdAboveImages = true;

    boolean searchRecursive = true;

    String currentImage = null;

    FileChooser fileChooser;
    Button searchFile;

    private enum State {
        DRAW, ROTATE
    }

    private State state = State.DRAW;

    public TilesetViewerScreen(MyGame game) {
        super(game);

        loadedTextures = new Array<>();
        imagesAndId = new ObjectMap<>();
        textureToName = new ObjectMap<>();

        // load settings
        Preferences preferences = Gdx.app.getPreferences("gdx.tileSetViewer");
        scale = preferences.getInteger("scale", scale);
        tileWidth = preferences.getInteger("tileWidth", tileWidth);
        tileHeight = preferences.getInteger("tileHeight", tileHeight);
        seeIdAboveImages = preferences.getBoolean("seeIdAboveImages", seeIdAboveImages);

        VisUI.load();
        FileChooser.setDefaultPrefsName("gdx.tileSetViewer.fileChooser");

        fileChooser = new FileChooser(FileChooser.Mode.OPEN);
        fileChooser.setSelectionMode(FileChooser.SelectionMode.DIRECTORIES);
        fileChooser.setListener(new FileChooserAdapter() {
            @Override
            public void selected (Array<FileHandle> file) {
                pathField.setText(file.first().path());
                loadPath(pathField.getText());
            }
        });


        createHud();

        pathField.setText(preferences.getString("pathField", ""));
        loadPath(pathField.getText());

        getHudController().stage.setDebugAll(MyGame.DEBUG);
    }

    private class TileGrid extends Table {
        int sizeX = 10;
        int sizeY = 10;

        int[][] rows;
        float[][] rotations;
        public TileGrid() {
            rows = new int[sizeY][sizeX];
            rotations = new float[sizeY][sizeX];

            for (int y = 0; y < sizeY; y++) {
                for (int x = 0; x < sizeX; x++) {
                    rows[y][x] = 0;
                    rotations[y][x] = 0;
                }
            }
        }

        void update() {
            clearChildren();

            for (int y = 0; y < sizeY; y++) {
                for (int x = 0; x < sizeX; x++) {
                    int id = rows[y][x];
                    float rotation = rotations[y][x];
                    ClickListener clickListener = new ClickListener(Input.Buttons.LEFT);
                    TileImage image = new TileImage(id, x, y, clickListener, rotation);
                    add(image);
                }
                row();
            }
        }

        private class TileImage extends Stack {
            Drawable defaultDrawable;
            float defaultRotation = 0;
            private ClickListener clickListener;
            private boolean wasOver = false;
            private Image image;

            private int x, y;
            private boolean justClicked = state != State.ROTATE;

            public TileImage(int id, int x, int y, ClickListener clickListener, float defaultRotation) {
                super();

                this.x = x;
                this.y = y;

                image = new Image(getRepresentativeTextureFromId(id));
                this.clickListener = clickListener;
                this.defaultDrawable = image.getDrawable();
                image.addListener(clickListener);
                image.setSize(tileWidth * scale, tileHeight * scale);
                add(image);

                if (seeIdAboveImages)
                    add(new ExtendedLabel(String.valueOf(id), getAssets().fontOutline.font));

                addListener(clickListener);

                updateOrigin();

                image.setRotation(defaultRotation);
                this.defaultRotation = defaultRotation;
            }

            @Override
            public void act(float delta) {
                super.act(delta);

                if (clickListener.isOver()) {
                    if (!wasOver) {
                        image.setColor(Color.GOLD);
                        if (state == State.DRAW) {
                            image.setDrawable(new TextureRegionDrawable(new TextureRegion(getRepresentativeTextureFromId(
                                    currentImage == null ? -1 : imagesAndId.get(currentImage)))));
                        } else if (state == State.ROTATE) {
                            //updateOrigin();
                        }
                        wasOver = true;
                    }

                    // button pressed
                    if (Gdx.input.isButtonPressed(clickListener.getButton())) {
                        if (justClicked) {
                            if (state == State.DRAW) {
                                rows[y][x] = currentImage == null ? -1 : imagesAndId.get(currentImage);
                            } else if (state == State.ROTATE) {
                                defaultRotation -= 90;
                                wasOver = false;
                                updateOrigin();
                                rotations[y][x] = defaultRotation;
                                System.out.println(defaultRotation);
                            }
                            update();
                        }
                    } else {
                        justClicked = true;
                    }
                } else {
                    wasOver = false;
                    image.setDrawable(defaultDrawable);
                    image.setColor(Color.WHITE);
                }
            }

            private void updateOrigin() {
                image.setOriginX(image.getWidth() / 2f);
                image.setOriginY(image.getHeight() / 2f);
            }

            @Override
            public float getPrefHeight() {
                return tileHeight * scale;
            }

            @Override
            public float getPrefWidth() {
                return tileWidth * scale;
            }
        }
    }

    private TextureRegion getRepresentativeTextureFromId(int id) {
        Texture texture;
        if (id == -1) {
            texture = null; // force pixel texture for this id
        } else {
            texture = getLoadedTextureFromId(id);
        }
        return (texture != null ? new TextureRegion(texture) : new TextureRegion(getAssets().findRegion("pixel")));
    }

    ExtendedScrollPane idSelectionList;
    ExtendedScrollPane toolWindow;
    Button rotateButton, drawButton;
    void createHud() {
        Skin skin = getAssets().skin;

        TileGrid paneTable = new TileGrid();
        tilesPane = new ExtendedScrollPane(paneTable, skin, true, false, false, true);

        Table toolWindowContents = new Table();
        toolWindow = new ExtendedScrollPane(toolWindowContents, skin, true, false, true, true);
        getHudController().centerLeftTable.right();
        getHudController().centerLeftTable.add(toolWindow).right();

        getHudController().centerLeftTable.bottom();
        getHudController().centerLeftTable.add(tilesPane).expand().bottom().right();

        Table loadedImagesTable = new Table();
        loadedImagesPane = new ExtendedScrollPane(loadedImagesTable, skin, true, false, true, true);
        loadedImagesPane.setScrollingDisabled(true, false);
        loadedImagesTable.padRight(3).padLeft(10);

        getHudController().centerRightTable.bottom().left();
        getHudController().centerRightTable.add(loadedImagesPane).left().bottom();

        Table idSelectionContents = new Table();
        idSelectionList = new ExtendedScrollPane(idSelectionContents, skin, true, false, true, true);
        idSelectionContents.padRight(10).padLeft(10);
        getHudController().centerRightTable.add(idSelectionList).left().bottom();

        drawButton = new Button(skin);
        drawButton.add(new Image(new Texture(Gdx.files.internal("atlas/textures_finished/draw.png"))));

        drawButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                state = State.DRAW;

                updateWholeUI();
            }
        });

        rotateButton = new Button(skin);
        rotateButton.add(new Image(new Texture(Gdx.files.internal("atlas/textures_finished/rotate.png"))));

        rotateButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                state = State.ROTATE;

                updateWholeUI();
            }
        });

        toolWindowContents.add(drawButton);
        toolWindowContents.row();
        toolWindowContents.add(rotateButton);

        pathField = new TextField("", skin);
        Table bottomTextFieldTable = new Table();

        Table optionsTable = new Table();
        ScrollPane optionsPane = new ExtendedScrollPane(optionsTable, skin, true, true, false, true);
        getHudController().downTable.add(optionsPane).expand().fill().top();
        optionsPane.setScrollingDisabled(false, true);

        String sizeX = "Tile width: ";
        String sizeY = "Tile height: ";
        ExtendedLabel tileSizeX = new ExtendedLabel(sizeX + tileWidth, getAssets().fontDefault.font_small);
        ExtendedLabel tileSizeY = new ExtendedLabel(sizeY + tileHeight, getAssets().fontDefault.font_small);

        MoreLessButtons tileSizeXButtons = new MoreLessButtons(skin, getAssets().fontDefault.font_small) {
            @Override
            public void moreAction() {
                tileWidth++;
                tileSizeX.setText(sizeX + tileWidth);
                updateWholeUI();
            }

            @Override
            public void lessAction() {
                tileWidth--;
                tileSizeX.setText(sizeX + tileWidth);
                updateWholeUI();
            }
        };

        MoreLessButtons tileSizeYButtons = new MoreLessButtons(skin, getAssets().fontDefault.font_small) {
            @Override
            public void moreAction() {
                tileHeight++;
                tileSizeY.setText(sizeY + tileHeight);
                updateWholeUI();
            }

            @Override
            public void lessAction() {
                tileHeight--;
                tileSizeY.setText(sizeY + tileHeight);
                updateWholeUI();
            }
        };

        optionsTable.add(tileSizeX).width(100);
        optionsTable.add(tileSizeXButtons).padLeft(10).padRight(20);

        optionsTable.add(tileSizeY).width(100);
        optionsTable.add(tileSizeYButtons).padLeft(10).padRight(20);

        String scaleText = "scale: ";
        ExtendedLabel scaleLabel = new ExtendedLabel(scaleText + scale, getAssets().fontDefault.font_small);
        MoreLessButtons scaleButtons = new MoreLessButtons(skin, getAssets().fontDefault.font_small) {
            @Override
            public void moreAction() {
                scale++;
                scaleLabel.setText(scaleText + scale);
                updateWholeUI();
            }

            @Override
            public void lessAction() {
                scale--;
                scaleLabel.setText(scaleText + scale);
                updateWholeUI();
            }
        };

        optionsTable.add(scaleLabel);
        optionsTable.add(scaleButtons).padLeft(10).padRight(20);

        String seeIdText = "see id above tiles? ";
        ExtendedLabel seeId = new ExtendedLabel(seeIdText + seeIdAboveImages, getAssets().fontDefault.font_small);
        Button changeSeeId = new LabelButton("change", getAssets().fontDefault.font_small, skin);
        changeSeeId.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                seeIdAboveImages = !seeIdAboveImages;
                seeId.setText(seeIdText + seeIdAboveImages);
                updateWholeUI();
            }
        });

        optionsTable.add(seeId).pad(0, 15, 0, 0);
        optionsTable.add(changeSeeId);

        getHudController().downTable.row();

        idsTable = new Table();
        //getHudController().downTable.add(idsTable);
        //getHudController().downTable.row();

        getHudController().downTable.add(bottomTextFieldTable).fillX().expandX().bottom();
        bottomTextFieldTable.add(pathField).fillX().expandX();

        Button clear = new LabelButton("clear", getAssets().fontDefault.font_small, skin);
        clear.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                pathField.setText("");
            }
        });
        bottomTextFieldTable.add(clear);

        confirmPath = new LabelButton("Look path", getAssets().fontDefault.font_small, skin);
        bottomTextFieldTable.add(confirmPath);
        confirmPath.setColor(Color.CHARTREUSE);
        confirmPath.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                loadPath(pathField.getText());
            }
        });

        searchFile = new LabelButton("Open...", getAssets().fontDefault.font_medium, skin);
        searchFile.setColor(Color.GOLD);
        searchFile.addListener(new ChangeListener() {
            @Override
            public void changed (ChangeEvent event, Actor actor) {
                fileChooser.setDirectory(pathField.getText());
                getStage().addActor(fileChooser.fadeIn());
                fileChooser.setSize(Math.min(Gdx.graphics.getWidth(), 500), Math.min(Gdx.graphics.getHeight(), 555));
            }
        });
        bottomTextFieldTable.add(searchFile);

        Gdx.input.setInputProcessor(stage);

        updateWholeUI();
    }

    private void loadPath(String path) {
        loadedTextures.clear();
        textureToName.clear();

        try {
            searchFolder(path);
        } catch (Exception exc) {

        }

        updateIdInfoWhenLoadingNewImages();
        updateWholeUI();
    }

    private void updateIdInfoWhenLoadingNewImages() {
        for (String name : imagesAndId.keys()) {
            if (getLoadedTextureFromName(name) == null)
                imagesAndId.remove(name);
        }

        for (Texture texture : loadedTextures) {
            String name = getNameFromTexture(texture);
            if (!imagesAndId.containsKey(name)) {
                imagesAndId.put(name, -1);
            }
        }
    }

    private Texture getLoadedTextureFromName(String name) {
        for (Texture image : loadedTextures) {
            String nameFromTexture = getNameFromTexture(image);
            if (nameFromTexture != null && nameFromTexture.equals(name)) {
                return image;
            }
        }

        return null;
    }

    private Texture getLoadedTextureFromId(int id) {
        for (ObjectMap.Entry<String, Integer> entry : imagesAndId.entries()) {
            if (entry.value == id) {
                return getLoadedTextureFromName(entry.key);
            }
        }

        return null;
    }

    private String getNameFromTexture(Texture texture) {
        return textureToName.get(texture, null);
    }

    private void searchFolder(String path) {
        for (FileHandle entry : Gdx.files.absolute(path).list()) {
            if (entry.isDirectory() && searchRecursive) {
                searchFolder(entry.path());
            } else if (entry.extension().equals("png")) {
                Texture texture = new Texture(entry);
                loadedTextures.add(texture);
                textureToName.put(texture, entry.nameWithoutExtension());
            }
        }
    }

    private void updateWholeUI() {
        if (state == State.DRAW) {
            drawButton.setColor(Color.GOLD);
            rotateButton.setColor(Color.WHITE);
        } else {
            drawButton.setColor(Color.WHITE);
            rotateButton.setColor(Color.GOLD);
        }

        // update tiles
        ((TileGrid) tilesPane.getActor()).update();

        // update loaded images list
        ((WidgetGroup) loadedImagesPane.getActor()).clearChildren();
        for (Actor actor : destroyableActors) {
            actor.remove();
        }
        destroyableActors.clear();

        for (Texture texture : loadedTextures) {
            // buttons of texture list
            Table table = (Table) loadedImagesPane.getActor();

            Button button = new Button(getAssets().skin);

            button.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    currentImage = getNameFromTexture(texture);
                    updateWholeUI();
                }
            });
            Table insideButtonTable = new Table();
            insideButtonTable.setClip(true);
            button.add(insideButtonTable).left().fill().expand().fill();

            insideButtonTable.add(new Image(texture)).size(tileWidth * scale, tileHeight * scale).left();
            insideButtonTable.add(new ExtendedLabel(getNameFromTexture(texture), getAssets().fontDefault.font_small)).left().expandX();
            insideButtonTable.right();
            insideButtonTable.add(new ExtendedLabel(imagesAndId.get(getNameFromTexture(texture)).toString(),
                    getAssets().fontDefault.font_big)).pad(0, 10, 0, 0).right();

            if (imagesAndId.get(getNameFromTexture(texture)) == -1) {
                button.setColor(Color.LIGHT_GRAY);
            }

            if (texture.getWidth() != tileWidth || texture.getHeight() != tileHeight) {
                insideButtonTable.row();
                insideButtonTable.add(new ExtendedLabel("doesn't have same size \nas specified tile size", getAssets().fontDefault.font_small))
                        .colspan(11);
                button.setColor(new Color(0.7f, 0.2f, 0.2f, 1));
            }

            if (currentImage != null && currentImage.equals(getNameFromTexture(texture))) {
                button.setColor(Color.GOLD);
            }

            table.add(button).padTop(5).padRight(10);
            table.row();
        }

        // table with buttons for each id number
        Table idSelectionContents = (Table) idSelectionList.getActor();
        idSelectionContents.clearChildren();
        for (int i = 0; i < 20; i++) {
            Button button = new LabelButton(String.valueOf(i), getAssets().fontDefault.font_medium, getAssets().skin);

            if (currentImage != null) {
                if (i == imagesAndId.get(currentImage)) {
                    button.setColor(Color.GOLD);
                } else {
                    if (getLoadedTextureFromId(i) != null) {
                        button.setColor(new Color(1f, 0.92f, 0.77f, 1));
                    }
                    int finalI = i;
                    button.addListener(new ChangeListener() {
                        @Override
                        public void changed(ChangeEvent event, Actor actor) {
                            changeIdOnImage(currentImage, finalI);
                            updateWholeUI();
                        }
                    });
                }
            } else {
                button.setDisabled(true);
                button.setColor(Color.GRAY);
            }
            idSelectionContents.add(button).size(55, 32);
            idSelectionContents.row();
        }
    }

    private void changeIdOnImage(String image, int id) {
        for (ObjectMap.Entry<String, Integer> entry : imagesAndId) {
            if (entry.value == id) {
                imagesAndId.put(entry.key, -1);
            }

            if (entry.key.equals(image)) {
                imagesAndId.put(image, id);
            }
        }

        updateWholeUI();
    }

    @Override
    public void pause() {
        super.hide();

        // save config
        Preferences preferences = Gdx.app.getPreferences("gdx.tileSetViewer");
        preferences.putInteger("scale", scale);
        preferences.putInteger("tileWidth", tileWidth);
        preferences.putInteger("tileHeight", tileHeight);
        preferences.putBoolean("seeIdAboveImages", seeIdAboveImages);
        preferences.putString("pathField", pathField.getText());
        preferences.flush();
    }

    @Override
    public void update(float delta) {
        super.update(delta);

        stage.act();
    }

    Color background = Utils.getColorFrom255RGB(109, 122, 123, 1);

    @Override
    public void draw(GameDrawer gameDrawer, float delta) {
        Gdx.gl.glClearColor(background.r, background.b, background.g, background.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }
}
