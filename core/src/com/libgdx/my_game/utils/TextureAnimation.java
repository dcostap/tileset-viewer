package com.libgdx.my_game.utils;

import com.badlogic.gdx.utils.Array;

/**
 * Created by Darius on 17/01/2018
 */
public class TextureAnimation<T> {
    private T[] frames;
    private float elapsedTime = 0;
    private boolean paused = false;
    private float frameDuration;
    private float totalAnimationDuration;
    private Type type;

    public enum Type {
        NORMAL, REVERSED, NORMAL_REVERSED_LOOP, STOP_IN_EACH_NEW_FRAME
    }

    private Type normalReversedCycle = Type.NORMAL;

    private boolean finishedOneLoop = false;

    public boolean stayOnLastFrameAfterFinished = false;

    public TextureAnimation(float frameDuration, T[] frames) {
        this.frames = frames;
        this.frameDuration = frameDuration;
        this.type = Type.NORMAL;
        updateTotalAnimationDuration();
    }

    public TextureAnimation(float frameDuration, Array<T> frames) {
        this(frameDuration, frames.toArray());
    }

    private void updateTotalAnimationDuration() {
        this.totalAnimationDuration = frameDuration * frames.length;
    }

    public T getFrame(float delta) {
        if (!paused) {
            switch (type) {
                case NORMAL:
                    elapsedTime += delta;
                    if (finishedNormalAnimation()) {
                        elapsedTime = 0;
                        finishedOneLoop = true;
                    }
                    break;

                case REVERSED:
                    elapsedTime -= delta;
                    if (finishedReversedAnimation()) {
                        elapsedTime = totalAnimationDuration - 0.0001f;
                        finishedOneLoop = true;
                    }
                    break;

                case NORMAL_REVERSED_LOOP:
                    if (normalReversedCycle == Type.NORMAL) {
                        elapsedTime += delta;
                        if (finishedNormalAnimation()) {
                            normalReversedCycle = Type.REVERSED;
                            setElapsedTime(getTotalAnimationDuration() - getFrameDuration() - 0.001f);
                        }
                    } else if (normalReversedCycle == Type.REVERSED) {
                        elapsedTime -= delta;
                        if (finishedReversedAnimation()) {
                            normalReversedCycle = Type.NORMAL;
                            setElapsedTime(getFrameDuration());
                            finishedOneLoop = true;
                        }
                    }
                    break;
                case STOP_IN_EACH_NEW_FRAME:
                    int previousFrame = getFrameIndex();
                    elapsedTime += delta;
                    int newFrame = getFrameIndex();
                    if (newFrame != previousFrame) {
                        pauseAnimation();
                        setFrame(newFrame);
                    }

                    if (finishedNormalAnimation()) {
                        finishedOneLoop = true;
                    }

                    break;
            }
        }

        return getFrameFromElapsedTime();
    }

    /** Whether the animation finished playing forward <p />
     * More detailed: Returns true when the elapsed time is bigger than maximum elapsed time (depending on
     * number of frames and frame duration) */
    public boolean finishedNormalAnimation() {
        return (elapsedTime >= totalAnimationDuration);
    }

    /** Whether the animation finished playing backwards <p />
     * More detailed: Returns true when the elapsed time is smaller than 0*/
    public boolean finishedReversedAnimation() {
        return (elapsedTime <= 0);
    }

    /** Starts counting loops since the last reset. What a loop is depends on type of animation. <i>(So, for example, in a
     * normalReversed animation, a loop is counted when the animation goes forward and backwards 1 time</i><p />
     *
     * <b>Warning: is represented by an internal boolean instead of the elapsed time, which means that if the
     * animation is paused the boolean value will not be updated, even if the animation technically ended <p />
     * Use {@link #finishedNormalAnimation()} or {@link #finishedReversedAnimation()} instead</b> */
    public boolean hasFinishedOneLoop() {
        return finishedOneLoop;
    }

    private T getFrameFromElapsedTime() {
        int index = getFrameIndex();
        return frames[index];
    }

    public int getFrameIndex() {
        int index;
        index = (int) Math.floor(elapsedTime / frameDuration);
        index = Math.max(index, 0);
        index = Math.min(index, frames.length - 1);
        return index;
    }

    public void resetAnimation() {
        elapsedTime = 0;
        finishedOneLoop = false;
    }

    public void pauseAnimation() {
        paused = true;
    }

    public void resumeAnimation() {
        paused = false;
    }

    public void setFrame(int frameIndex) {
        elapsedTime = getFrameDuration() * frameIndex;
        finishedOneLoop = false;
    }

    public int getNumberOfFrames() {
        return frames.length;
    }

    public boolean isPaused() {
        return paused;
    }

    public float getFrameDuration() {
        return frameDuration;
    }

    public void setFrameDuration(float frameDuration) {
        this.frameDuration = frameDuration;

        updateTotalAnimationDuration();
    }

    public float getTotalAnimationDuration() {
        return totalAnimationDuration;
    }

    public T[] getFrames() {
        return frames;
    }

    public float getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(float elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
