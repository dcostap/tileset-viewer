package com.libgdx.my_game.utils;

import com.badlogic.gdx.graphics.Color;

/**
 * Created by Darius on 09/01/2018
 */
public class FlashingThing implements Updatable {

    private boolean flash = false;
    private Color flash_color = Color.WHITE;
    private float flash_duration = 0;
    private float alphaLimit = 0;

    public void flashColor(Color color, float duration, float alphaLimit) {
        this.flash = true;
        this.flash_color = color;
        this.flash_duration = duration;
        this.alphaLimit = alphaLimit;
    }

    public void flashColor(Color color, float duration) {
        this.flash = true;
        this.flash_color = color;
        this.flash_duration = duration;
        this.alphaLimit = 0.55f;
    }

    public void update(float delta) {
        if (flash) {
            flash_duration -= delta;
            if (flash_duration <= 0) {
                flash = false;
            }
        }
    }

    public boolean isFlashing() {
        return flash;
    }

    public void setupGameDrawer(GameDrawer gameDrawer) {
        gameDrawer.setAlpha(Math.min(flash_duration, alphaLimit));
        gameDrawer.setColor(flash_color);
    }

    public void resetGameDrawer(GameDrawer gameDrawer) {
        gameDrawer.resetColor();
        gameDrawer.resetAlpha();
    }

}
