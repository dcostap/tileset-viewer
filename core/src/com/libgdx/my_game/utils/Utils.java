package com.libgdx.my_game.utils;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonValue;

import java.util.Random;

/**
 * Created by Darius on 11/11/2017.
 */
public class Utils {
    static private Random rand = new Random();

    static public void moveCameraTopDownToCoordinate(OrthographicCamera camera, float x, float y) {
        camera.position.x = x + (camera.viewportWidth * camera.zoom) / 2f;
        camera.position.y = y + (camera.viewportHeight * camera.zoom) / 2f;
    }

    static public float getDecimalPart(float number) {
        return (number - (int) number);
    }

    static public float percentage(float number, float percentage) {
        return number * (percentage / 100f);
    }
    static public int percentageInt(float number, float percentage) {
        return (int) (number * (percentage / 100f));
    }

    static public String removeExtensionFromFilename(String filename) {
        int i = filename.lastIndexOf(".");
        if (i >= 0)
            return filename.substring(0, i);
        else
            return filename;
    }

    static public String getFileName(String filename) {
        String[] paths = filename.split("/");
        return paths[paths.length - 1];
    }

    static public void modifyScene2dActorToBlockInputBeneathItself(Actor actor) {
        actor.setTouchable(Touchable.enabled);

        actor.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {

            }
        });
    }

    /** Puts the actor in the center of the stage. Call .pack() on the actor before; needs to know its final size */
    public static void centerActor(Actor actor, Stage stage) {
        actor.setPosition((int) ((stage.getWidth() - actor.getWidth()) / 2f),
                (int) ((stage.getHeight() - actor.getHeight()) / 2f));
    }

    /**
     * Includes 0, doesn't include length (From 0 to length - 1)
     */
    static public int getRandomInteger(int length) {
        return rand.nextInt(length);
    }

    static public float getRandomFloat() {
        return rand.nextFloat();
    }

    static public float clamp(float val, float min, float max) {
        if (val < min) return min;
        if (val > max) return max;
        return val;
    }

    static public boolean getChanceToBeTrue(float percentageOfTrue) {
        if (percentageOfTrue < 0 || percentageOfTrue > 100)
            throw new IllegalArgumentException("Invalid percentage value: " + percentageOfTrue);

        return getRandomFloat() < (percentageOfTrue / 100f);
    }

    /**
     * Modifies input Array
     */
    static public void getRangeOfIntegers(int minValue, int maxValue, Array<Integer> arrayToPopulate) {
        if (minValue == maxValue) {
            return;
        } else if (minValue > maxValue)
            throw new IllegalArgumentException("getRangeOfIntegers, minValue: " + minValue
                    + "is bigger than maxValue: " + maxValue);

        for (int i = minValue; i <= maxValue; i++) {
            arrayToPopulate.add(i);
        }
    }

    static public float getRandomFloatInsideRange(float positiveMin, float positiveMax, boolean randomSign) {
        if (positiveMax == positiveMin) return positiveMax;
        if (positiveMax < positiveMin)
            throw new IllegalArgumentException("Invalid input floats: " + positiveMax + " isn't" +
                    "bigger than " + positiveMin);

        return mapNumberToRange(getRandomFloat(), 0, 1, positiveMin, positiveMax)
                * (randomSign ? (Utils.getChanceToBeTrue(50) ? 1 : -1) : 1);
    }

    /**
     * clamps input values to input range, then maps them output range
     * outputLow may be bigger than outputHigh, map still will happen correctly
     */
    static public float mapNumberToRange(float inputNumber, float inputLow, float inputHigh, float outputLow, float outputHigh) {
        if (inputNumber < inputLow) return outputLow;
        if (inputNumber > inputHigh) return outputHigh;

        boolean switched = false;
        if (outputLow > outputHigh) {
            float temp = outputHigh;
            outputHigh = outputLow;
            outputLow = temp;
            switched = true;
        }

        float scale = (outputHigh - outputLow) / (inputHigh - inputLow);
        float value = (inputNumber - inputLow) * scale + outputLow;

        if (switched) {
            return (outputLow - value) + outputHigh;
        }

        return value;
    }

    static public float lerp(float point1, float point2, float alpha) {
        return point1 + alpha * (point2 - point1);
    }

    static public float getXFromDirectionMovement(float rawSpeed, float directionDegrees) {
        if (directionDegrees < 0) {
            directionDegrees += 360;
        }
        return rawSpeed * (float) Math.cos(Math.toRadians(directionDegrees));
    }

    static public float getYFromDirectionMovement(float rawSpeed, float directionDegrees) {
        if (directionDegrees < 0) {
            directionDegrees += 360;
        }
        return rawSpeed * (float) Math.sin(Math.toRadians(directionDegrees));
    }

    /**
     * Length (angular) of a shortest way between two angles.
     * It will be in range [-180, 180] (signed).
     */
    static public float getAngleDifferenceSigned(float sourceAngle, float targetAngle) {
        sourceAngle = (float) Math.toRadians(sourceAngle);
        targetAngle = (float) Math.toRadians(targetAngle);
        return (float) Math.toDegrees(Math.atan2(Math.sin(targetAngle - sourceAngle), Math.cos(targetAngle - sourceAngle)));
    }

    static public float getAngleDifferenceNotSigned(float sourceAngle, float targetAngle) {
        return Math.abs(getAngleDifferenceSigned(sourceAngle, targetAngle));
    }

    static public float getAngleBetweenPoints(Vector2 start, Vector2 end) {
        float angle = (float) Math.toDegrees(Math.atan2(end.y - start.y, end.x - start.x));

        if (angle < 0) {
            angle += 360;
        }

        return angle;
    }

    static public float getAngleBetweenPoints(float x1, float y1, float x2, float y2) {
        float angle = (float) Math.toDegrees(Math.atan2(y2 - y1, x2 - x1));

        if (angle < 0) {
            angle += 360;
        }

        return angle;
    }

    static public float getDistanceBetweenPoints(float x1, float y1, float x2, float y2) {
        return (float) Math.hypot(x1 - x2, y1 - y2);
    }

    static public float getDistanceBetweenPoints(Vector2 point1, Vector2 point2) {
        return getDistanceBetweenPoints(point1.x, point1.y, point2.x, point2.y);
    }

    static public Color getColorFrom255RGB(int red, int green, int blue, float alpha) {
        return new Color(red / 255f, green / 255f, blue / 255f, alpha);
    }
}
