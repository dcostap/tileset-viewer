package com.libgdx.my_game.utils.finite_state_machine;

import java.util.HashMap;

/**
 * Created by Darius on 31/08/2017.
 * <p />
 * A custom implementation of a Finite State Machine
 * There may be only one globalState and one state executing at the same time.
 * However there can be any number of subStates executing at the same time.
 */
public class StateMachine<T> {
    public State<T> currentState;
    public State<T> previousState;
    private T owner;
    private GlobalState<T> globalState;

    private float delay = 0;

    private HashMap<SubState<T>, Boolean> subStates = new HashMap<>();

    public StateMachine(T owner, State<T> currentState, GlobalState<T> globalState) {
        this.owner = owner;
        this.currentState = currentState;
        this.globalState = globalState;
    }

    public void update(float delta) {
        if (delay > 0) {
            delay = Math.max(0, delay - delta);
            return;
        }

        if (globalState != null) {
            globalState.execute(owner, delta);
        }

        for (HashMap.Entry<SubState<T>, Boolean> entry : subStates.entrySet()) {
            if (entry.getValue()) {
                entry.getKey().execute(owner, delta);
            }
        }

        if (currentState != null) {
            currentState.execute(owner, delta);
        }
    }

    public void changeState(State<T> newState) {
        previousState = currentState;
        currentState.exit(owner);
        currentState = newState;
        currentState.enter(owner);
    }

    /**
     * Enters the indicated subState; if it is already executing, first exits it then enters (resets the subState).
     * If you want to set some variables before starting the new subState, make sure to call the exit method before, since
     * variables are normally reset on exit.
     */
    public void setSubState(SubState<T> subState) {
        if (subStates.getOrDefault(subState, false)) {
            subState.exit(owner);
        }

        subStates.put(subState, true);
        subState.enter(owner);
    }

    public void exitSubState(SubState<T> subState) {
        if (subStates.getOrDefault(subState, false)) {
            subStates.put(subState, false);
            subState.exit(owner);
        }
    }

    public void exitAllSubStates() {
        for (SubState<T> subState : subStates.keySet()) {
            exitSubState(subState);
        }
    }

    public boolean isSubStateSet(SubState<T> subState) {
        return subStates.getOrDefault(subState, false);
    }

    public void revertToPreviousState() {
        changeState(previousState);
    }
}
