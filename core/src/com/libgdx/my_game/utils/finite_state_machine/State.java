package com.libgdx.my_game.utils.finite_state_machine;

/**
 * Created by Darius on 31/08/2017.
 */
public interface State<T> {
    void enter(T being);

    void execute(T being, float delta);

    void exit(T being);

    String getDebugInfo(T being);
}
