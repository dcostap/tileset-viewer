package com.libgdx.my_game.utils;

/**
 * Created by Darius on 23/11/2017
 */
public interface Killable {
    void kill();

    boolean isKilled();
}
