package com.libgdx.my_game.utils;

/**
 * Created by Darius on 29/11/2017
 */
public class AlphaIncreasingTexture {

    public double maxAlpha;
    public double startingAlpha;

    private double drawingAlpha;

    private boolean decreased = false;
    private boolean increased = false;

    public AlphaIncreasingTexture(double maxAlpha, double startingAlpha) {
        this.maxAlpha = maxAlpha;
        this.startingAlpha = startingAlpha;

        this.drawingAlpha = startingAlpha;
    }

    public void increaseAlpha(float amountPerSecond, float delta) {
        decreased = false;
        if (increased) return;
        drawingAlpha += delta * amountPerSecond;

        if (drawingAlpha > maxAlpha) {
            drawingAlpha = maxAlpha;
            increased = true;
        }
    }

    public void decreaseAlpha(float amountPerSecond, float delta) {
        increased = false;
        if (decreased) return;
        drawingAlpha -= delta * amountPerSecond;

        if (drawingAlpha < startingAlpha) {
            drawingAlpha = startingAlpha;
            decreased = true;
        }
    }

    public float getDrawingAlpha() {
        return (float) drawingAlpha;
    }

    public void resetAlpha() {
        decreased = true;
        increased = false;

        drawingAlpha = startingAlpha;
    }

}
