package com.libgdx.my_game.utils;

/**
 * Created by Darius on 15/11/2017
 */
public interface Drawable {
    void draw(GameDrawer gameDrawer, float delta);
}
