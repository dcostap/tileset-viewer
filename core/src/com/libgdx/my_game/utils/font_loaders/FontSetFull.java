package com.libgdx.my_game.utils.font_loaders;

import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * Created by Darius on 04/01/2018
 */
public class FontSetFull implements FontSet {
    public BitmapFont font_verySmall;
    public BitmapFont font_small;
    public BitmapFont font_medium;
    public BitmapFont font_big;
    public BitmapFont font_veryBig;

    // default sizes
    private int verySmallSize = 14;
    private int smallSize = 16;
    private int mediumSize = 20;
    private int bigSize = 26;
    private int veryBigSize = 32;

    public FontSetFull(int verySmallSize, int smallSize, int mediumSize, int bigSize, int veryBigSize) {
        this.verySmallSize = verySmallSize;
        this.smallSize = smallSize;
        this.mediumSize = mediumSize;
        this.bigSize = bigSize;
        this.veryBigSize = veryBigSize;
    }

    /** use default sizes */
    public FontSetFull() {

    }

    public int getVerySmallSize() {
        return verySmallSize;
    }

    public int getSmallSize() {
        return smallSize;
    }

    public int getMediumSize() {
        return mediumSize;
    }

    public int getBigSize() {
        return bigSize;
    }

    public int getVeryBigSize() {
        return veryBigSize;
    }
}
