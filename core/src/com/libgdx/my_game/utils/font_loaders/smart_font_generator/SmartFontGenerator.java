package com.libgdx.my_game.utils.font_loaders.smart_font_generator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.glutils.PixmapTextureData;
import com.badlogic.gdx.utils.*;

/** Heavily adapted from jrenner's SmartFontGenerator */
public class SmartFontGenerator {
	private static final String TAG = "SmartFontGenerator";
	private static String generatedFontDir = "fonts/generated";
	private static String jsonFontsInfoName = "generatedFonts.json";
	private static int pageSize = 512; // size of atlas pages for font pngs
    private static JsonReader jsonReader = new JsonReader();

    /** Version is saved on each generated font's info. Change it to force a reload of fonts on startup
     * The code will erase all generated fonts from the folder, performing cleanup */
    private static String fontVersion = "1.0";

    public static BitmapFont loadFontOrGenerateIt(String fontIdentifier,
                                                  FreeTypeFontGenerator generator, FreeTypeFontParameter parameter)
    {
        boolean generateIt = false;

        try {
            JsonValue wholeFile = jsonReader.parse(Gdx.files.local(generatedFontDir + "/" + jsonFontsInfoName));

            if (!wholeFile.getString("version").equals(fontVersion)) {
                System.out.println("Different version from global version; erasing all generated fonts");
                Gdx.files.local(generatedFontDir).deleteDirectory();
                generateIt = true;
            }
            if (wholeFile.getInt("resolutionWidth") != (Gdx.graphics.getWidth())
                    || wholeFile.getInt("resolutionHeight") != (Gdx.graphics.getHeight()))
            {
                System.out.println("Different resolution from last time; erasing all generated fonts");
                Gdx.files.local(generatedFontDir).deleteDirectory();
                generateIt = true;
            }

            FreeTypeFontParameter fileParameter = loadParameterFromJson(wholeFile.get(fontIdentifier));

            if (!areParametersEqual(fileParameter, parameter)) {
                System.out.println("--> .json file parameters are different; generating font");
                generateIt = true;
            }
        } catch (Exception exception) {
            System.out.println("--> couldn't load fonts .json file; generating font");
            generateIt = true;
        }

        // try to load the previously generated font
        if (!generateIt) {
            try {
                BitmapFont font = new BitmapFont(Gdx.files.local(generatedFontDir + "/" + fontIdentifier + ".fnt"));
                System.out.println("--> font was already generated; font loaded");
                return font;
            } catch (Exception exception) {
                System.out.println("--> Error while loading already generated font: " + fontIdentifier);
                System.out.println(exception.getMessage());
                System.out.println();
            }
        }

        // generate the font

        System.out.println("--> generating the font...");
        // add to the json file with info from generated fonts
        // parameter info
        JsonValue jsonValue = new JsonValue(JsonValue.ValueType.object);
        jsonValue.name = fontIdentifier;
        jsonValue.addChild("size", new JsonValue(parameter.size));
        jsonValue.addChild("borderWidth", new JsonValue(parameter.borderWidth));
        jsonValue.addChild("borderColor", new JsonValue(parameter.borderColor.toString()));
        jsonValue.addChild("color", new JsonValue(parameter.color.toString()));

        JsonValue wholeFile;
        String file = generatedFontDir + "/" + jsonFontsInfoName;
        try {
            // already exists a .json file?
            wholeFile = jsonReader.parse(Gdx.files.local(file));
        } catch (Exception exception) {
            // create new .json
            wholeFile = new JsonValue(JsonValue.ValueType.object);
            wholeFile.name = "generatedFontsProperties";
        }

        // if properties info from the font already exist, delete it
        // todo: remove generated stuff from previous font; currently it is left stored (changing the fontVersion cleans it though)
        if (wholeFile.get(fontIdentifier) != null) {
            wholeFile.remove(fontIdentifier);
        }
        // add new properties of the font to the .json
        wholeFile.addChild(jsonValue);

        if (wholeFile.get("version") == null) {
            wholeFile.addChild("version", new JsonValue(fontVersion));
        } else {
            wholeFile.get("version").set(fontVersion);
        }
        if (wholeFile.get("resolutionWidth") == null) {
            wholeFile.addChild("resolutionWidth", new JsonValue(String.valueOf(Gdx.graphics.getWidth())));
        } else {
            wholeFile.get("resolutionWidth").set(String.valueOf(Gdx.graphics.getWidth()));
        }
        if (wholeFile.get("resolutionHeight") == null) {
            wholeFile.addChild("resolutionHeight", new JsonValue(String.valueOf(Gdx.graphics.getHeight())));
        } else {
            wholeFile.get("resolutionHeight").set(String.valueOf(Gdx.graphics.getHeight()));
        }

        // write .json
        FileHandle fileHandle = Gdx.files.local(file);
        fileHandle.writeString(wholeFile.toJson(JsonWriter.OutputType.minimal), false);

        System.out.println("    -> saved new font info on .json file: " + jsonFontsInfoName);
        System.out.println("    -> font generated");

        // generate it
        return generateFontWriteFiles(fontIdentifier, generator, parameter);
    }

    private static FreeTypeFontParameter loadParameterFromJson(JsonValue parameterJson) {
        FreeTypeFontParameter parameter = new FreeTypeFontParameter();

        parameter.size = parameterJson.getInt("size");
        parameter.borderWidth = parameterJson.getInt("borderWidth");
        parameter.borderColor = Color.valueOf(parameterJson.getString("borderColor"));
        parameter.color = Color.valueOf(parameterJson.getString("color"));

        return parameter;
    }

    private static boolean areParametersEqual(FreeTypeFontParameter parameter1, FreeTypeFontParameter parameter2) {
        return (parameter1.color.equals(parameter2.color) && parameter1.borderColor.equals(parameter2.borderColor)
                && parameter1.borderWidth == parameter2.borderWidth && parameter1.size == parameter2.size);
    }

    /**
     * Convenience method for generating a font, and then writing the fnt and png files.
     * Writing a generated font to files allows the possibility of only generating the fonts when they are missing, otherwise
     * loading from a previously generated file.
     */
    private static BitmapFont generateFontWriteFiles(String fontIdentifier, FreeTypeFontGenerator generator, FreeTypeFontParameter parameter) {
        PixmapPacker packer = new PixmapPacker(pageSize , pageSize, Pixmap.Format.RGBA8888, 2, false);
        parameter.packer = packer;
        FreeTypeFontGenerator.FreeTypeBitmapFontData fontData = generator.generateData(parameter);
        Array<PixmapPacker.Page> pages = packer.getPages();
        Array<TextureRegion> texRegions = new Array<>();
        for (int i = 0; i < pages.size; i++) {
            PixmapPacker.Page p = pages.get(i);
            Texture tex = new Texture(
                    new PixmapTextureData(p.getPixmap(), p.getPixmap().getFormat(), false, false, true)) {
                @Override
                public void dispose() {
                    super.dispose();
                    getTextureData().consumePixmap().dispose();
                }
            };
            tex.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
            texRegions.add(new TextureRegion(tex));
        }
        BitmapFont font = new BitmapFont(fontData, texRegions, false);
        saveFontToFile(font, parameter.size, fontIdentifier, packer);
        packer.dispose();
        return font;
    }

    private static void saveFontToFile(BitmapFont font, int fontSize, String fontIdentifier, PixmapPacker packer) {
        FileHandle fontFile = Gdx.files.local(generatedFontDir + "/" + fontIdentifier + ".fnt"); // .fnt path
        FileHandle pixmapDir = Gdx.files.local(generatedFontDir + "/" + fontIdentifier); // png dir path
        BitmapFontWriter.setOutputFormat(BitmapFontWriter.OutputFormat.Text);

        String[] pageRefs = BitmapFontWriter.writePixmaps(packer.getPages(), pixmapDir, fontIdentifier);
        Gdx.app.debug(TAG, String.format("Saving font [%s]: fontfile: %s, pixmapDir: %s\n", fontIdentifier, fontFile, pixmapDir));
        // here we must add the png folder to the page refs
        for (int i = 0; i < pageRefs.length; i++) {
            pageRefs[i] = fontIdentifier + "/" + pageRefs[i];
        }
        BitmapFontWriter.writeFont(font.getData(), pageRefs, fontFile, new BitmapFontWriter.FontInfo(fontIdentifier, fontSize), 1, 1);
    }
}