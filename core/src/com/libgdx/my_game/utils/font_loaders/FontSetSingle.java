package com.libgdx.my_game.utils.font_loaders;

import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * Created by Darius on 04/01/2018
 */
public class FontSetSingle implements FontSet {
    public BitmapFont font;

    // default sizes
    private int uniqueSize = 20;

    public FontSetSingle(int uniqueSize) {
        this.uniqueSize = uniqueSize;
    }

    /** use default sizes */
    public FontSetSingle() {

    }

    public int getUniqueSize() {
        return uniqueSize;
    }
}
