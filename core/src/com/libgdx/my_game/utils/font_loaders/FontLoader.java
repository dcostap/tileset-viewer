package com.libgdx.my_game.utils.font_loaders;

import com.badlogic.gdx.assets.AssetManager;

/**
 * Created by Darius on 04/01/2018
 */
public interface FontLoader {
    /**
     * uses the libgdx AssetManager to load the font files. Call before finishing loading assets with AssetManager!
     */
    void loadFonts(AssetManager assetManager);

    /**
     * actually retrieves the final font / fonts, applying any configuration. <p />
     * call after loading the font files in the assetManager!
     */
    void finishLoading(AssetManager assetManager);
}
