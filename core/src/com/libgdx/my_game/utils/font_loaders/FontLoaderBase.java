package com.libgdx.my_game.utils.font_loaders;

import com.badlogic.gdx.assets.AssetManager;

/**
 * Created by Darius on 04/01/2018
 */
public abstract class FontLoaderBase implements FontLoader {
    private String fontLocation;
    private String fontName;
    private String fontFolder;


    public FontLoaderBase(String fontFolder, String fontName) {
        this.fontLocation = fontFolder + "/" + fontName;
        this.fontName = fontName;
        this.fontFolder = fontFolder;
    }

    public String getFontFolder() {
        return fontFolder;
    }

    public String getFontName() {
        return fontName;
    }

    protected String getFontLocation() {
        return fontLocation;
    }

    @Override
    public abstract void loadFonts(AssetManager assetManager);

    @Override
    public abstract void finishLoading(AssetManager assetManager);
}