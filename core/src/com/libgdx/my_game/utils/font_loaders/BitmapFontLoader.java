package com.libgdx.my_game.utils.font_loaders;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.BitmapFontLoader.BitmapFontParameter;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * Created by Darius on 04/01/2018
 * <p/>
 * Loads one BitmapFont
 */
public class BitmapFontLoader extends FontLoaderBase {
    private BitmapFont font;

    public BitmapFontLoader(String fontFolder, String fontName) {
        super(fontFolder, fontName);
    }

    @Override
    public void loadFonts(AssetManager assetManager) {
        BitmapFontParameter parameter = new BitmapFontParameter();
        parameter.magFilter = Texture.TextureFilter.Linear;
        parameter.minFilter = Texture.TextureFilter.Linear;

        assetManager.load(getFontLocation(), BitmapFont.class, parameter);
    }

    @Override
    public void finishLoading(AssetManager assetManager) {
        font = assetManager.get(getFontLocation(), BitmapFont.class);
    }

    public BitmapFont getFont() {
        return font;
    }
}
