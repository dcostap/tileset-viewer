package com.libgdx.my_game.utils.font_loaders;

import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * Created by Darius on 04/01/2018
 */
public class FontSetNormal implements FontSet {
    public BitmapFont font_small;
    public BitmapFont font_medium;
    public BitmapFont font_big;

    // default sizes
    private int smallSize = 16;
    private int mediumSize = 20;
    private int bigSize = 26;

    public FontSetNormal(int smallSize, int mediumSize, int bigSize) {
        this.smallSize = smallSize;
        this.mediumSize = mediumSize;
        this.bigSize = bigSize;
    }

    /** use default sizes */
    public FontSetNormal() {

    }

    public int getSmallSize() {
        return smallSize;
    }

    public int getMediumSize() {
        return mediumSize;
    }

    public int getBigSize() {
        return bigSize;
    }
}
