package com.libgdx.my_game.utils.font_loaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.libgdx.my_game.MyGame;
import com.libgdx.my_game.utils.font_loaders.smart_font_generator.SmartFontGenerator;

/**
 * Created by Darius on 04/01/2018
 * <p/>
 * Generates BitmapFonts dynamically from one TrueTypeFont. Sizes generated are determined from the FontSizes object passed <p />
 * Can be configured to use density factor, so that the sizes passed are transformed by the factor, so they might
 * actually be bigger or smaller size
 */
public class FreeTypeFontLoader extends FontLoaderBase {
    private FreeTypeFontGenerator generator;
    private FreeTypeFontGenerator.FreeTypeFontParameter parameter;

    private boolean useDensityFactor;

    /** will scale up based on base resolution */
    private boolean useResolutionFactor;
    /** if using resolution factor, this width value will correspond to scale factor 1x */
    private int baseResolutionWidth;

    private FontSet fontSet;

    private boolean useSmartFontGenerator = true;

    /** Must be different for each generated font, so that each one is cached separated, even if loading from the same .ttf file
     * That is, this variable gives a name to each generated font */
    private String fontIdentifier;

    private FreeTypeFontLoader(String fontFolder, String fontName, FontSet fontSet, boolean useDensityFactor,
                               boolean useResolutionFactor, int baseResolutionWidth, String fontIdentifier,
                               FreeTypeFontGenerator.FreeTypeFontParameter parameter)
    {
        super(fontFolder, fontName);

        this.fontSet = fontSet;
        this.useDensityFactor = useDensityFactor;
        this.useResolutionFactor = useResolutionFactor;
        this.baseResolutionWidth = baseResolutionWidth;
        this.fontIdentifier = fontIdentifier;

        this.parameter = parameter;

        if (this.parameter == null) {
            this.parameter = getDefaultGeneratorParameter();
        }

    }

    /** use density factor to scale the generated fonts size */
    public FreeTypeFontLoader(String fontFolder, String fontName, String fontIdentifier,
                              FontSet fontSet, boolean useDensityFactor,
                              FreeTypeFontGenerator.FreeTypeFontParameter parameter)
    {
        this(fontFolder, fontName, fontSet, useDensityFactor, false, 0, fontIdentifier, parameter);
    }

    /** use resolution factor to scale the generated fonts size */
    public FreeTypeFontLoader(String fontFolder, String fontName, String fontIdentifier, FontSet fontSet,
                              boolean useResolutionFactor, int baseResolutionWidth,
                              FreeTypeFontGenerator.FreeTypeFontParameter parameter)
    {
        this(fontFolder, fontName, fontSet, false, useResolutionFactor, baseResolutionWidth, fontIdentifier, parameter);
    }

    /**
     * Loads the TrueType (.ttf) fonts
     */
    @Override
    public void loadFonts(AssetManager assetManager) {
        // font loading
        assetManager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(new InternalFileHandleResolver()));
        assetManager.load(getFontLocation(), FreeTypeFontGenerator.class);
    }

    @Override
    public void finishLoading(AssetManager assetManager) {
        generator = assetManager.get(getFontLocation(), FreeTypeFontGenerator.class);
        generateFonts();
    }

    /**
     * Override on children to generate the fonts on specific sizes with specific config
     */
    private void generateFonts() {
        System.out.println("Loading font: " + getFontName() + "; id: " + fontIdentifier + "...");
        if (fontSet instanceof FontSetFull) {
            FontSetFull fontSetFull = (FontSetFull) fontSet;
            fontSetFull.font_verySmall = generateFontFromSize(fontSetFull.getVerySmallSize());
            fontSetFull.font_small = generateFontFromSize(fontSetFull.getSmallSize());
            fontSetFull.font_medium = generateFontFromSize(fontSetFull.getMediumSize());
            fontSetFull.font_big = generateFontFromSize(fontSetFull.getBigSize());
            fontSetFull.font_veryBig = generateFontFromSize(fontSetFull.getVeryBigSize());
        }
        if (fontSet instanceof FontSetNormal) {
            FontSetNormal fontSetNormal = (FontSetNormal) fontSet;
            fontSetNormal.font_small = generateFontFromSize(fontSetNormal.getSmallSize());
            fontSetNormal.font_medium = generateFontFromSize(fontSetNormal.getMediumSize());
            fontSetNormal.font_big = generateFontFromSize(fontSetNormal.getBigSize());
        }
        if (fontSet instanceof FontSetSingle) {
            FontSetSingle fontSetSingle = (FontSetSingle) fontSet;
            fontSetSingle.font = generateFontFromSize(fontSetSingle.getUniqueSize());
        }

        System.out.println("_____");
    }

    /**
     * Overwrites the size of the parameter config, for ease of use when creating multiple fonts with different sizes
     */
    private BitmapFont generateFontFromSize(int baseSize) {
        System.out.println("     size: " + baseSize);
        parameter.size = getSizeAdaptedToFactors(baseSize);

        if (useSmartFontGenerator) {
            return SmartFontGenerator.loadFontOrGenerateIt(fontIdentifier + "_" + baseSize, generator, parameter);
        } else {
            return generator.generateFont(parameter);
        }
    }

    public static FreeTypeFontGenerator.FreeTypeFontParameter getGeneratorParameterFromConfig(Color fontColor,
                                                                                     float baseBorderSize,
                                                                                     Color borderColor)
    {
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.borderWidth = baseBorderSize;
        parameter.borderColor = borderColor;
        parameter.color = fontColor;

        return parameter;
    }

    public static FreeTypeFontGenerator.FreeTypeFontParameter getDefaultGeneratorParameter() {
        return getGeneratorParameterFromConfig(Color.WHITE, 0, Color.WHITE);
    }

    /** adapts the base size to the factors chosen for this generator: no factors / adapted from base
     * resolution / adapted from density factor */
    private int getSizeAdaptedToFactors(float baseSize) {
        return (int) (baseSize * (useDensityFactor ? MyGame.getDensityFactor() :
                (useResolutionFactor ? Gdx.graphics.getWidth() / (float) baseResolutionWidth : 1)));
    }
}
