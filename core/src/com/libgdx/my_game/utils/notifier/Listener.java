package com.libgdx.my_game.utils.notifier;

/**
 * Created by Darius on 18/10/2017.
 */
public interface Listener<T> {
    void notify(T event);
}
