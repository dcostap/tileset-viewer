package com.libgdx.my_game.utils.notifier;

import com.badlogic.gdx.utils.Array;

/**
 * Created by Darius on 06/09/2017.
 * <p>
 * Common class for all notifiers
 */
public class Notifier<T> {
    protected Array<Listener<T>> listeners = new Array<>();

    public void registerObserver(Listener<T> listener) {
        listeners.add(listener);
    }

    public void removeObserver(Listener<T> listener) {
        listeners.removeValue(listener, false);
    }

    public void notifyObservers(T event) {
        for (Listener<T> listener : listeners) {
            listener.notify(event);
        }
    }
}
