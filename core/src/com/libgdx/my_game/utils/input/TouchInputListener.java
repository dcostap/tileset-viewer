package com.libgdx.my_game.utils.input;

/**
 * Created by Darius on 19/11/2017.
 */
public interface TouchInputListener {
    /**
     * Called when the touch is first pressed until it is released
     *
     * @param screenX with origin on bottom-left
     * @param screenY with origin on bottom-left
     * @param pointer ID of the finger pressed; always 0 if it's a mouse in desktop
     *                Various events can be issued at the same time, one for each pointer;
     *                ignore pointers other than 0 if you don't want to potentially have repeated calls of the same method
     */
    void touchDownEvent(float screenX, float screenY, float worldX, float worldY, int button, int pointer, boolean isJustPressed);

    /**
     * @see #touchDownEvent(float, float, float, float, int, int, boolean)
     */
    void touchReleasedEvent(float screenX, float screenY, float worldX, float worldY, int button, int pointer);
}
