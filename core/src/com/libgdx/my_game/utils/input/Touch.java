package com.libgdx.my_game.utils.input;

/**
 * Created by Darius on 20/01/2018
 */
class Touch {
    protected float screenX;
    protected float screenY;
    protected float worldX;
    protected float worldY;
    protected int button;
    protected boolean isJustPressed;

    public Touch(float screenX, float screenY, float worldX, float worldY, int button, boolean isJustPressed) {
        this.screenX = screenX;
        this.screenY = screenY;
        this.worldX = worldX;
        this.worldY = worldY;
        this.button = button;
        this.isJustPressed = isJustPressed;
    }

    public void set(float screenX, float screenY, float worldX, float worldY) {
        this.screenX = screenX;
        this.screenY = screenY;
        this.worldX = worldX;
        this.worldY = worldY;
    }
}
