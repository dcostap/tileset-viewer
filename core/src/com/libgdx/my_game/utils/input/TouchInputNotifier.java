package com.libgdx.my_game.utils.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.libgdx.my_game.utils.Updatable;

/**
 * Created by Darius on 19/11/2017.
 * <p />
 * Notifies adapted touch events, with input coordinates projected to one Viewport.
 * <p />
 * Detailed: Notifies input (touch down and released) projected into the Viewport's coords, and also screen raw coords.
 * <b>Must be set as InputProcessor.</b>
 */
public class TouchInputNotifier extends InputAdapter implements Updatable {
    private Viewport worldViewport;
    private Vector2 dummyVector = new Vector2();

    private Array<TouchInputListener> listeners = new Array<>();

    private ObjectMap<Integer, Touch> touches = new ObjectMap<>();

    public TouchInputNotifier(Viewport worldViewport) {
        this.worldViewport = worldViewport;
    }

    /**
     * Listeners are notified on this method.
     */
    @Override
    public void update(float delta) {
        for (ObjectMap.Entry<Integer, Touch> entry : touches) {
            for (TouchInputListener listener : listeners) {
                listener.touchDownEvent(entry.value.screenX, entry.value.screenY, entry.value.worldX, entry.value.worldY,
                        entry.value.button, entry.key, entry.value.isJustPressed);
            }

            // reset justPressed flag
            entry.value.isJustPressed = false;
        }
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        dummyVector.set(screenX, screenY);
        dummyVector = screenToViewportCoordinates(dummyVector);

        //System.out.println("INPUT CONTROLLER: TOUCHDOWN RECEIVED, listeners number: " + listeners.size);

        touches.put(pointer, new Touch(screenX, Gdx.graphics.getHeight() - screenY,
                dummyVector.x, dummyVector.y, button, true));

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        dummyVector.set(screenX, screenY);
        dummyVector = screenToViewportCoordinates(dummyVector);

        touches.remove(pointer);

        for (TouchInputListener listener : listeners) {
            listener.touchReleasedEvent(screenX, Gdx.graphics.getHeight() - screenY,
                    dummyVector.x, dummyVector.y, button, pointer);
        }

        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        dummyVector.set(screenX, screenY);
        dummyVector = screenToViewportCoordinates(dummyVector);

        Touch touch = touches.get(pointer);
        touch.set(screenX, Gdx.graphics.getHeight() - screenY,
                dummyVector.x, dummyVector.y);

        return false;
    }

    /**
     * Warning: Modifies input vector
     */
    public Vector2 screenToViewportCoordinates(Vector2 screenCoords) {
        worldViewport.unproject(screenCoords);
        return screenCoords;
    }

    public void registerObserver(TouchInputListener listener) {
        listeners.add(listener);
    }

    public void removeObserver(TouchInputListener listener) {
        if (listeners.contains(listener, false))
            listeners.removeValue(listener, false);
    }
}
