package com.libgdx.my_game.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pools;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.libgdx.my_game.Assets;
import com.libgdx.my_game.MyGame;
import com.libgdx.my_game.engine.hud.utils.ScaledImage;

/**
 * Created by Darius on 14/09/2017.
 * <p>
 * Helper class to draw stuff in a specific viewport. By default it adapts textures drawn to PPM.
 * <p />
 * Use it with a <b>scaling viewport</b> with arbitrary units (so 1 unit = PPM pixels):
 * <ul>
 * <li> Automatically scales all textures to the PPM, so when drawing 1 pixel isn't 1 unit</li>
 * Note: you shouldn't draw fonts in scaling viewports, do it in another viewport
 * </ul>
 * <p />
 * Use it with a <b>ScreenViewport</b> that adjusts to different resolutions using DensityFactor
 * (so higher resolution != smaller images, and so physical size of drawn things stays the same):
 * <ul>
 * <li>Use drawScaled to draw images that scale to comply with DensityFactor, based on an initial size</li>
 * <li>Draw normally with a font, if that font is generated in Assets with size based on DensityFactor</li>
 * </ul>
 *
 * If you use <b>Scene2d</b>, adapt to density factor this way:
 * <ul>
 * <li> use stage's actor {@link ScaledImage} to draw images, will work the same way as using drawScaled</li>
 * <li> with 9patches (button / pane / widget graphics), create them with a base size multiplied by MyGame.getDensityFactor</li>
 * <li>use fonts the same way as before :D</li>
 * </ul>
 *
 * <b>If you don't want it to scale textures based on the PPM</b> (scaling viewport with no arbitrary units),
 * you should have a global PPM of 1, or change the PPM used only in this
 * class to be 1.
 */
public class GameDrawer {

    private Batch gameMainBatch;
    private Assets assets;
    private TextureRegion pixel_spr;

    private float alpha = 1;
    private Color color = Color.WHITE;

    private Viewport worldViewport;

    private Vector2 drawingDisplacement = new Vector2(0, 0);

    private Array<ObjectMap.Entry<String, Vector2>> textStack = new Array<>();

    private int PPM;

    /**
     * @param batch The main batch used in the game, that the GameDrawer will use to draw
     */
    public GameDrawer(Batch batch, Assets assets, Viewport worldViewport) {
        this.gameMainBatch = batch;
        this.worldViewport = worldViewport;
        this.assets = assets;

        pixel_spr = assets.getTextureAtlas().findRegion("pixel");

        if (pixel_spr == null)
            throw new RuntimeException("pixel texture not found on textureAtlas");

        PPM = MyGame.PPM;
    }

    /** Use this to change how images are scaled in the base draw methods. By default they are scaled to MyGame's PPM,
     * which should be the value used everywhere. <p />
     * Example of use: if you want to not scale images based on the value, because you are using a ScreenViewport for example, set
     * this PPM to 1. */
    public void setPPM(int PPM) {
        this.PPM = PPM;
    }

    /**
     * Gets the nearest non-decimal scale factor for the image, based on the DensityFactor of the app
     */
    public static int getImageScaleFactor(float size, float imageWidthPixels, float imageHeightPixels) {
        int pixels = (int) (MyGame.getDensityFactor() * size);

        return Math.round((float) pixels / Math.max(imageHeightPixels, imageWidthPixels));
    }

    /** input translated to game units */
    public void setDrawingDisplacementPixels(float x, float y) {
        this.setDrawingDisplacementX(x / PPM);
        this.setDrawingDisplacementY(y / PPM);
    }

    public void setDrawingDisplacement(float x, float y) {
        this.drawingDisplacement.set(x, y);
    }

    public void setDrawingDisplacementX(float x) {
        this.drawingDisplacement.x = x;
    }

    public void setDrawingDisplacementXY(float xy) {
        this.drawingDisplacement.set(xy, xy);
    }

    public void setDrawingDisplacementY(float y) {
        this.drawingDisplacement.y = y;
    }

    public void resetDrawingDisplacement() {
        this.setDrawingDisplacementXY(0);
    }

    public void resetColorAndAlpha() {
        this.resetAlpha();
        this.resetColor();
    }

    public Viewport getWorldViewport() {
        return worldViewport;
    }

    public void setWorldViewport(Viewport worldViewport) {
        this.worldViewport = worldViewport;
    }

    private void updateDrawingColorAndAlpha() {
        gameMainBatch.setColor(color.r, color.g, color.b, alpha);
    }

    public void setColor(Color color) {
        this.color = color;
        updateDrawingColorAndAlpha();
    }

    public void resetColor() {
        this.color = Color.WHITE;
        updateDrawingColorAndAlpha();
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
        updateDrawingColorAndAlpha();
    }

    public void resetAlpha() {
        this.alpha = 1;
        updateDrawingColorAndAlpha();
    }

    public void draw(TextureRegion textureRegion, float x, float y, float rotation) {
        this.draw(textureRegion, x, y, rotation, 0, 0);
    }

    public void draw(TextureRegion textureRegion, float x, float y) {
        this.draw(textureRegion, x, y, 0);
    }

    public void draw(TextureRegion textureRegion, Vector2 position) {
        this.draw(textureRegion, position.x, position.y, 0);
    }

    public void draw(TextureRegion textureRegion, float x, float y, float rotation, float originX, float originY) {
        this.draw(textureRegion, x, y, rotation, 1, 1, originX, originY);
    }

    public void draw(TextureRegion textureRegion, float x, float y, float rotation, float scaleX, float scaleY, float originX, float originY) {
        x = getCorrectX(x);
        y = getCorrectY(y);

        float width = getUnitWidth(textureRegion);
        float height = getUnitHeight(textureRegion);

        gameMainBatch.draw(textureRegion, x, y, originX, originY,
                width, height, scaleX, scaleY, rotation);
    }

    public void draw(TextureRegion textureRegion, float x, float y, float width, float height, float rotation, float scaleX, float scaleY, float originX, float originY) {
        x = getCorrectX(x);
        y = getCorrectY(y);

        gameMainBatch.draw(textureRegion, x, y, originX, originY,
                width, height, scaleX, scaleY, rotation);
    }

    /**
     * Automatically sets the origin in the center of the texture, either on X, Y or both. This allows for scaling &
     * rotation to happen around that origin. <p />
     * Position of drawing will <b>not</b> be at origin, but still at X and Y
     * coordinates specified. To draw the image centered, that is, having the center of the texture being the X and Y
     * coordinates, use {@link #drawCentered(TextureRegion, float, float, float, float, boolean, boolean, float)}
     */
    public void drawWithOriginOnCenter(TextureRegion textureRegion, float x, float y, float scaleX, float scaleY,
                                       boolean centerOnXAxis, boolean centerOnYAxis, float rotationDegrees) {
        x = getCorrectX(x);
        y = getCorrectY(y);

        float width = getUnitWidth(textureRegion);
        float height = getUnitHeight(textureRegion);

        gameMainBatch.draw(textureRegion, x, y, !centerOnXAxis ? 0 : (width / 2f),
                !centerOnYAxis ? 0 : (height / 2f),
                width, height, scaleX, scaleY, rotationDegrees);
    }

    public void drawWithOriginOnCenter(TextureRegion textureRegion, float x, float y, boolean horizontalMirror,
                                       boolean centerOriginOnXAxis, boolean centerOriginOnYAxis, float rotationDegrees) {
        this.drawWithOriginOnCenter(textureRegion, x, y, horizontalMirror ? -1 : 1, 1, centerOriginOnXAxis, centerOriginOnYAxis, rotationDegrees);
    }

    /**
     * Offsets the drawing by half of its size so the origin of the input X and Y is in the middle of the texture, thus
     * the texture is being drawn centered on input X and Y
     */
    public void drawCentered(TextureRegion textureRegion, float x, float y, float scaleX, float scaleY,
                             boolean centerOnXAxis, boolean centerOnYAxis, float rotationDegrees) {
        this.drawCenteredWithOriginOnCenter(textureRegion, x, y, scaleX, scaleY, centerOnXAxis, centerOnYAxis, false, false, rotationDegrees);
    }

    /**
     * Combines centering of origin with centering of image. Origin centered allows for rotation and scaling to happen around
     * it. Centering of image allows the image to be drawn with input X and Y being in its middle, thus centered on those
     * coordinates.
     */
    public void drawCenteredWithOriginOnCenter(TextureRegion textureRegion, float x, float y, float scaleX, float scaleY,
                                               boolean centerOnXAxis, boolean centerOnYAxis,
                                               boolean centerOriginOnXAxis, boolean centerOriginOnYAxis, float rotationDegrees) {
        float width = getUnitWidth(textureRegion);
        float height = getUnitHeight(textureRegion);

        setDrawingDisplacementX(centerOnXAxis ? -width / 2f : 0);
        setDrawingDisplacementY(centerOnYAxis ? -height / 2f : 0);

        x = getCorrectX(x);
        y = getCorrectY(y);

        gameMainBatch.draw(textureRegion, x, y, !centerOriginOnXAxis ? 0 : (width / 2f), !centerOriginOnYAxis ? 0 : (height / 2f),
                width, height, scaleX, scaleY, rotationDegrees);

        setDrawingDisplacementXY(0);
    }

    public void drawCenteredWithOriginOnCenter(TextureRegion textureRegion, Vector2 position, float scaleX, float scaleY,
                                               boolean centerOnXAxis, boolean centerOnYAxis,
                                               boolean centerOriginOnXAxis, boolean centerOriginOnYAxis, float rotationDegrees) {
        this.drawCenteredWithOriginOnCenter(textureRegion, position.x, position.y, scaleX, scaleY, centerOnXAxis, centerOnYAxis,
                centerOriginOnXAxis, centerOriginOnYAxis, rotationDegrees);
    }

    public void drawCenteredWithOriginOnCenter(TextureRegion textureRegion, Vector2 position, float scaleXY,
                                               boolean centerOnXAxis, boolean centerOnYAxis,
                                               boolean centerOriginOnXAxis, boolean centerOriginOnYAxis, float rotationDegrees) {
        this.drawCenteredWithOriginOnCenter(textureRegion, position.x, position.y, scaleXY, scaleXY, centerOnXAxis, centerOnYAxis,
                centerOriginOnXAxis, centerOriginOnYAxis, rotationDegrees);
    }

    /**
     * Draws an image scaled to the nearest non-decimal scale factor, based on the DensityFactor of the app
     * This means that the images will -almost- always have the same physical size on all devices
     * <p>
     * Use it with ScreenViewport - since with a scaling viewport the above can't be achieved
     */
    public void drawScaled(TextureRegion textureRegion, float x, float y, float baseSize) {
        x = getCorrectX(x);
        y = getCorrectY(y);

        float scaleFactor = getImageScaleFactor(baseSize, textureRegion.getRegionWidth(), textureRegion.getRegionHeight());

        gameMainBatch.draw(textureRegion, x, y, textureRegion.getRegionWidth() * scaleFactor,
                textureRegion.getRegionHeight() * scaleFactor);
    }

    private float getCorrectX(float x) {
        return x + drawingDisplacement.x;
    }

    private float getCorrectY(float y) {
        return y + drawingDisplacement.y;
    }

    public Batch getBatch() {
        return gameMainBatch;
    }

    /** returns the amount the texture needs to be scaled to be drawn according to the Pixels Per Meter (PPM) constant **/
    public float getUnitWidth(TextureRegion textureRegion) {
        return (float) textureRegion.getRegionWidth() / PPM;
    }

    public float getUnitHeight(TextureRegion textureRegion) {
        return (float) textureRegion.getRegionHeight() / PPM;
    }

    public void drawText(BitmapFont bitmapFont, float x, float y, String text) {
        bitmapFont.draw(gameMainBatch, text, x, y);
    }

    /**
     * Save text in a list, with the world coords. The coords will be projected onto the HUD screen
     * As a result the text is drawn with the scale of the HUD instead of the scale of the world viewport
     * Call {@link #drawStackOfTextFromWorldCoords(BitmapFont)} later when drawing HUD to draw all the saved text
     */
    public void drawTextFromWorldCoordsInHudCoords(Viewport worldViewport, float worldX, float worldY, String text) {
        ObjectMap.Entry<String, Vector2> entry = new ObjectMap.Entry<>();
        entry.key = text;
        Vector2 coords = Pools.obtain(Vector2.class);
        coords.set(worldX, worldY);
        worldViewport.project(coords);
        entry.value = coords;

        textStack.add(entry);
    }

    public boolean hasStackOfTextFromWorldCoordsToDraw() {
        return textStack.size > 0;
    }

    /**
     * Call after the batch used by this GameDrawer is setup to have HUD's projection
     */
    public void drawStackOfTextFromWorldCoords(BitmapFont font) {
        for (ObjectMap.Entry<String, Vector2> entry : textStack) {
            drawText(font, entry.value.x, entry.value.y, entry.key);
            Pools.free(entry.value);
        }

        textStack.clear();
    }

    /**
     * Draws a Rectangle using "pixel" image on atlas
     *
     * @param x         bottom-left corner x
     * @param y         bottom-left corner y
     * @param thickness size of the borders if the rectangle is not filled
     * @param fill      whether the rectangle drawn will be filled with the color
     */
    public void drawRectangle(float x, float y, float width, float height, float thickness, boolean fill) {
        x = getCorrectX(x);
        y = getCorrectY(y);

        if (!fill) {
            gameMainBatch.draw(pixel_spr, x, y, width, thickness);
            gameMainBatch.draw(pixel_spr, x, y, thickness, height);
            gameMainBatch.draw(pixel_spr, x, y + height - thickness, width, thickness);
            gameMainBatch.draw(pixel_spr, x + width - thickness, y, thickness, height);
        } else {
            gameMainBatch.draw(pixel_spr, x, y, width, height);
        }
    }

    public void drawRectangle(Rectangle rectangle, float thickness, boolean fill) {
        this.drawRectangle(rectangle.x, rectangle.y, rectangle.width, rectangle.height, thickness, fill);
    }

    /**
     * Draws a line using "pixel" image on atlas. Lines drawn together will not be correctly joined.
     *
     * @param x1        start x
     * @param y1        start y
     * @param x2        end x
     * @param y2        end y
     * @param thickness size of the line
     */
    public void drawLine(float x1, float y1, float x2, float y2, float thickness) {
        x1 = getCorrectX(x1);
        x2 = getCorrectX(x2);
        y1 = getCorrectY(y1);
        y2 = getCorrectY(y2);

        float dx = x2 - x1;
        float dy = y2 - y1;
        float dist = (float) Math.sqrt(dx * dx + dy * dy);
        float deg = (float) Math.toDegrees((float) Math.atan2(dy, dx));
        gameMainBatch.draw(pixel_spr, x1, y1, 0, thickness / 2f, dist, thickness, 1, 1, deg);
    }

    public void drawLineFromAngle(float x1, float y1, float distance, float angleDegrees, float thickness) {
        this.drawLine(x1, y1, (float) (x1 + distance * Math.cos(Math.toRadians(angleDegrees))),
                (float) (y1 + distance * Math.sin(Math.toRadians(angleDegrees))), thickness);
    }

    public void drawArrow(float x1, float y1, float x2, float y2, float thickness, float arrowSize) {
        drawLine(x1, y1, x2, y2, thickness);
        double angle = Math.toRadians(Utils.getAngleBetweenPoints(x1, y1, x2, y2));
        int angleDiff = -40;
        double angle1 = angle + angleDiff;
        double angle2 = angle - angleDiff;
        drawLine(x2, y2, x2 + (float) (Math.cos(angle1) * arrowSize), y2 + (float) (Math.sin(angle1) * arrowSize), thickness);
        drawLine(x2, y2, x2 + (float) (Math.cos(angle2) * arrowSize), y2 + (float) (Math.sin(angle2) * arrowSize), thickness);
    }

    public Assets getAssets() {
        return assets;
    }
}