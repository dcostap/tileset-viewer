package com.libgdx.my_game.utils;

/**
 * Created by Darius on 15/11/2017
 */
public interface Updatable {
    void update(float delta);
}
