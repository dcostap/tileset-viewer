package com.libgdx.my_game.utils.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.libgdx.my_game.utils.GameDrawer;
import com.libgdx.my_game.utils.input.TouchInputNotifier;
import com.libgdx.my_game.MyGame;
import com.libgdx.my_game.engine.hud.HudController;

/**
 * Created by Darius on 28/12/2017
 *
 * Adds base HUD functionality: HudController and a Stage
 */
public abstract class BaseScreenWithHud extends BaseScreen {

    protected Stage stage;
    private HudController hudController;

    public BaseScreenWithHud(MyGame game) {
        super(game);
    }

    @Override
    protected void create(MyGame game) {
        this.game = game;

        // set up and center the camera
        camera = new OrthographicCamera();
        camera.position.set(0, 0, 0);
        camera.zoom = 1f;

        createViewport();

        touchInputNotifier = new TouchInputNotifier(viewport);

        createStage();

        hudController = new HudController(getTouchInputNotifier(), getAssets(), stage);

        gameDrawer = new GameDrawer(game.getBatch(), game.getAssets(), viewport);

        InputMultiplexer inputMultiplexer = new InputMultiplexer(stage, touchInputNotifier);
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    /** Override to change viewport */
    protected void createViewport() {
        viewport = new ExtendViewport(MyGame.BASE_WIDTH / (float) MyGame.PPM, MyGame.BASE_HEIGHT / (float) MyGame.PPM, camera);
    }

    /** Override to change Stage viewport */
    protected void createStage() {
        stage = new Stage(new ScreenViewport());
    }

    public Stage getStage() {
        return stage;
    }

    public HudController getHudController() {
        return hudController;
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
    }
}
