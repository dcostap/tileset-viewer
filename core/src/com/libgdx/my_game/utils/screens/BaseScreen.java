package com.libgdx.my_game.utils.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.libgdx.my_game.Assets;
import com.libgdx.my_game.utils.GameDrawer;
import com.libgdx.my_game.MyGame;
import com.libgdx.my_game.utils.Drawable;
import com.libgdx.my_game.utils.Updatable;
import com.libgdx.my_game.utils.input.TouchInputNotifier;

/**
 * Convenience behavior already coded: base variables with MyGame, a Camera, a Viewport, TouchInputNotifier and GameDrawer
 * Extend this class instead of ScreenAdapter if you need all that functionality
 */
public abstract class BaseScreen implements Screen, Drawable, Updatable {
    protected MyGame game;
    protected OrthographicCamera camera;
    protected Viewport viewport;

    protected GameDrawer gameDrawer;
    protected TouchInputNotifier touchInputNotifier;

    public BaseScreen(MyGame game) {
        create(game);
    }

    @Override
    public abstract void draw(GameDrawer gameDrawer, float delta);

    @Override
    public void update(float delta) {
        touchInputNotifier.update(delta);
    }

    /**
     * Default implementation
     * Override if needed to change how the things are created (different viewports, etc)
     */
    protected void create(MyGame game) {
        this.game = game;

        // set up and center the camera
        camera = new OrthographicCamera();
        camera.position.set(0, 0, 0);
        camera.zoom = 1f;

        createViewport();

        touchInputNotifier = new TouchInputNotifier(viewport);

        Gdx.input.setInputProcessor(touchInputNotifier);

        gameDrawer = new GameDrawer(game.getBatch(), game.getAssets(), viewport);
    }

    /** Override to change viewport <p />
     * The viewport that is created by default is adapted to use the PPM value, so that in the viewport coordinates are units
     * instead of pixels. You should always create viewports like this: divide width and height by PPM. If you want 1 unit to be 1 pixel
     * set the PPM value to 1 */
    protected void createViewport() {
        viewport = new ExtendViewport(MyGame.BASE_WIDTH / (float) MyGame.PPM, MyGame.BASE_HEIGHT / (float) MyGame.PPM, camera);
    }

    @Override
    public void render(float delta) {
        update(delta);
        draw(gameDrawer, delta);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    public Assets getAssets() {
        return game.getAssets();
    }

    public MyGame getGame() {
        return game;
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public Viewport getViewport() {
        return viewport;
    }

    public TouchInputNotifier getTouchInputNotifier() {
        return touchInputNotifier;
    }

    public void pause() {
    }

    public void resume() {
    }

    public void show() {
    }

    public void hide() {
    }

    public void dispose() {
    }

}
