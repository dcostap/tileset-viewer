package com.libgdx.my_game.utils.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

/**
 * Created by Darius on 29/12/2017
 * <p>
 * Use to provide Screen functionality to any class.
 * For example, this way you can easily have Screens with multiple other Screens inside.
 * <p>
 * You can therefore have a hierarchy of Screens:
 * - GameScreen
 * - MapScreen
 * - MainScreen
 * - OptionsScreen
 */
public class ScreenManager {
    private Screen screen;

    /**
     * @return the currently active {@link Screen}.
     */
    public Screen getScreen() {
        return screen;
    }

    /**
     * Sets the current screen. {@link Screen#hide()} is called on any old screen, and {@link Screen#show()} is called on the new
     * screen, if any.
     *
     * @param screen may be {@code null}
     */
    public void setScreen(Screen screen) {
        if (this.screen != null) this.screen.hide();
        this.screen = screen;
        if (this.screen != null) {
            this.screen.show();
            this.screen.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        }
    }
}
