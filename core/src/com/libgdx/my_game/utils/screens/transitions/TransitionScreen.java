package com.libgdx.my_game.utils.screens.transitions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.libgdx.my_game.MyGame;

/**
 * Automatically switches from one Screen to another doing some effect in the middle
 */
public class TransitionScreen implements Screen {

    private MyGame app;
    private Screen prevScreen;
    private Screen nextScreen;
    private com.libgdx.my_game.utils.screens.transitions.TransitionEffect outEffect;
    private com.libgdx.my_game.utils.screens.transitions.TransitionEffect inEffect;
    private TransitionState currentState;

    public TransitionScreen(MyGame app, Screen prevScreen, Screen nextScreen,
                            com.libgdx.my_game.utils.screens.transitions.TransitionEffect outEffect, com.libgdx.my_game.utils.screens.transitions.TransitionEffect inEffect) {
        this.app = app;
        this.prevScreen = prevScreen;
        this.nextScreen = nextScreen;
        this.currentState = TransitionState.OUT;

        this.inEffect = inEffect;
        this.outEffect = outEffect;
    }

    @Override
    public void render(float delta) {
        switch (currentState) {
            case OUT:
                renderOutTransition(delta);
                break;
            case IN:
                renderInTransition(delta);
                break;
        }
    }

    private void renderOutTransition(float delta) {
        // Draw the previous screen and the effect on top of it
        prevScreen.render(delta);
        outEffect.render(delta);

        // Hide the previous screen, then prepare and show the next screen
        if (outEffect.isFinished()) {
            prevScreen.hide();
            currentState = TransitionState.IN;
            nextScreen.show();
            nextScreen.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        }
    }

    private void renderInTransition(float delta) {
        // Draw the next screen and the effect on top of it
        nextScreen.render(delta);
        inEffect.render(delta);

        if (inEffect.isFinished()) {
            app.setScreen(nextScreen);
        }
    }

    public void resize(int width, int height) {
        inEffect.resize(width, height);
        outEffect.resize(width, height);
    }

    public void pause() {
    }

    public void resume() {
    }

    public void show() {
    }

    public void hide() {
    }

    public void dispose() {
    }

    public enum TransitionState {
        IN, OUT
    }

}
