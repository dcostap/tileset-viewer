package com.libgdx.my_game.utils.screens.transitions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.libgdx.my_game.MyGame;

/**
 * Created by Darius on 03/01/2018
 */
public class SlideEffect extends com.libgdx.my_game.utils.screens.transitions.TransitionEffect {

    com.libgdx.my_game.utils.screens.transitions.TransitionScreen.TransitionState transitionState;
    float extraSeconds;
    private float secondsToFinishEffect;
    private Vector2 slidePosition = new Vector2();
    private boolean effectFinished = false;
    private float elapsedTime;

    public SlideEffect(MyGame myGame, float secondsToFinishEffect, float extraSeconds, com.libgdx.my_game.utils.screens.transitions.TransitionScreen.TransitionState transitionState) {
        super(myGame);

        this.secondsToFinishEffect = secondsToFinishEffect;
        this.transitionState = transitionState;
        this.extraSeconds = extraSeconds;

        if (transitionState == com.libgdx.my_game.utils.screens.transitions.TransitionScreen.TransitionState.OUT) {
            slidePosition.set(Gdx.graphics.getWidth(), 0);
        } else {
            slidePosition.set(0, 0);
        }
    }

    @Override
    public void render(float delta) {
        viewport.apply(true);

        elapsedTime += delta;

        effectFinished = elapsedTime >= secondsToFinishEffect;

        finished = elapsedTime >= secondsToFinishEffect + extraSeconds;

        if (!effectFinished) {
            float amount = (Gdx.graphics.getWidth() / secondsToFinishEffect) * delta;
            slidePosition.add(-amount, 0);
        } else {
            if (transitionState == com.libgdx.my_game.utils.screens.transitions.TransitionScreen.TransitionState.OUT) {
                slidePosition.set(0, 0);
            } else {
                slidePosition.set(Gdx.graphics.getWidth() * 2, 0);
            }
        }

        game.getBatch().setProjectionMatrix(viewport.getCamera().combined);
        game.getBatch().begin();

        gameDrawer.setColor(Color.BLACK);

        gameDrawer.drawRectangle(slidePosition.x, slidePosition.y, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0, true);

        gameDrawer.resetColor();
        game.getBatch().end();

    }
}
