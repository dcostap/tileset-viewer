package com.libgdx.my_game.utils.screens.transitions;

import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.libgdx.my_game.utils.GameDrawer;
import com.libgdx.my_game.MyGame;

public abstract class TransitionEffect {

    protected boolean finished;
    protected MyGame game;

    protected Viewport viewport;
    protected GameDrawer gameDrawer;

    public TransitionEffect(MyGame game) {
        this.game = game;
        this.finished = false;

        create();
    }

    /**
     * Default viewport and camera creation. Override to make changes on children.
     */
    protected void create() {
        viewport = new ScreenViewport();

        gameDrawer = new GameDrawer(game.getBatch(), game.getAssets(), viewport);
    }

    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    public abstract void render(float delta);

    public boolean isFinished() {
        return this.finished;
    }

}
