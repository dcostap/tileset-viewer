package com.libgdx.my_game.utils.screens.transitions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.libgdx.my_game.MyGame;
import com.libgdx.my_game.utils.Utils;

/**
 * Created by Darius on 03/01/2018
 */
public class FadeEffect extends com.libgdx.my_game.utils.screens.transitions.TransitionEffect {

    private float alpha;
    private float secondsToFinishEffect;

    private com.libgdx.my_game.utils.screens.transitions.TransitionScreen.TransitionState state;

    private float extraSecondsAfterFadeFinished;

    public FadeEffect(MyGame myGame, float secondsToFinishEffect, float extraSecondsAfterFadeFinished, com.libgdx.my_game.utils.screens.transitions.TransitionScreen.TransitionState state) {
        super(myGame);

        this.extraSecondsAfterFadeFinished = extraSecondsAfterFadeFinished;
        this.secondsToFinishEffect = secondsToFinishEffect;
        this.state = state;

        switch (state) {
            case OUT:
                alpha = 0;
                break;
            case IN:
                alpha = 1.2f;
                break;
        }
    }

    @Override
    public void render(float delta) {
        viewport.apply(true);

        game.getBatch().setProjectionMatrix(viewport.getCamera().combined);
        game.getBatch().begin();

        gameDrawer.setColor(Color.BLACK);

        float finalAlpha = Utils.clamp(alpha, 0, 1);
        gameDrawer.setAlpha(finalAlpha);

        gameDrawer.drawRectangle(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0, true);

        alpha += (delta / secondsToFinishEffect) * (state == com.libgdx.my_game.utils.screens.transitions.TransitionScreen.TransitionState.OUT ? 1 : -1);

        finished = ((state == com.libgdx.my_game.utils.screens.transitions.TransitionScreen.TransitionState.IN && alpha <= 0)
                || (state == com.libgdx.my_game.utils.screens.transitions.TransitionScreen.TransitionState.OUT && alpha >= 1 + extraSecondsAfterFadeFinished));

        gameDrawer.resetColor();

        game.getBatch().end();
    }
}
