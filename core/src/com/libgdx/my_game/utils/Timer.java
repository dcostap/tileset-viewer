package com.libgdx.my_game.utils;

/**
 * Created by Darius on 03/01/2018
 */
public class Timer {
    private float elapsed;
    private float timeLimit;
    private boolean isTimerOn;
    private boolean turnOffWhenTimeIsReached;

    public Timer(float timeLimit, boolean isTimerOn, boolean turnOffWhenTimeIsReached) {
        this.timeLimit = timeLimit;
        this.elapsed = 0;
        this.isTimerOn = isTimerOn;
        this.turnOffWhenTimeIsReached = turnOffWhenTimeIsReached;
    }

    /**
     * default looping behavior, timer on at start
     */
    public Timer(float timeLimit) {
        this(timeLimit, true, false);
    }

    /**
     * @return whether the timer reached the timeLimit
     */
    public boolean tick(float delta) {
        if (isTimerOn) {
            elapsed += delta;
            if (elapsed > timeLimit) {
                elapsed -= timeLimit;

                if (turnOffWhenTimeIsReached) {
                    turnTimerOff();
                }

                return true;
            }
        }

        return false;
    }

    public void resetTimer() {
        this.elapsed = 0;
    }

    public void turnTimerOff() {
        isTimerOn = false;
    }

    public void turnTimerOn() {
        isTimerOn = true;
    }

    public float getElapsed() {
        return elapsed;
    }

    public void setElapsed(float elapsed) {
        this.elapsed = elapsed;
    }

    public float getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(float timeLimit) {
        this.timeLimit = timeLimit;
    }

    public boolean isTimerOn() {
        return isTimerOn;
    }

    public boolean turnsOffWhenTimeIsReached() {
        return turnOffWhenTimeIsReached;
    }

    public void setTurnOffWhenTimeIsReached(boolean turnOffWhenTimeIsReached) {
        this.turnOffWhenTimeIsReached = turnOffWhenTimeIsReached;
    }
}
