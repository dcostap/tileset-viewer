package com.libgdx.my_game.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.libgdx.my_game.MyGame;

import java.io.*;
import java.util.Scanner;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = MyGame.BASE_WIDTH;
        config.height = MyGame.BASE_HEIGHT;
        config.vSyncEnabled = false;
        config.foregroundFPS = 60;
        config.backgroundFPS = 60;
        config.title = "TileSet Viewer";
        config.addIcon("atlas/textures_finished/icon1.png", Files.FileType.Internal);
        config.addIcon("atlas/textures_finished/icon2.png", Files.FileType.Internal);
        config.addIcon("atlas/textures_finished/icon3.png", Files.FileType.Internal);

        checkWhetherToPackImages();

        new LwjglApplication(new MyGame(), config);
    }

    public static String atlasExportedImagesFolder = "atlas/textures_finished";
    public static String atlasImagesHashFile = "atlas/textureHash.txt";

    /** Will use texturePacker to pack all images, only if files have changed or atlas output files are not created
     * Loads settings from pack.json files in folders
     * Gradle task texturePacker does the same packing, Android launcher won't pack so use the task if needed */
    public static void checkWhetherToPackImages() {
        System.out.println("_____\nChecking image hashes...");
        long initTime = System.currentTimeMillis();

        int hashingTotal = hashAllFiles(new File(atlasExportedImagesFolder));
        int c = 0;
        try {
            File hashFile = new File(atlasImagesHashFile);
            File atlasFile = new File("atlas/atlas.atlas");
            File atlasImageFile = new File("atlas/atlas.png");
            if (!hashFile.exists() || !atlasFile.exists() || !atlasImageFile.exists()) {
                PrintWriter pw = new PrintWriter(hashFile);
                pw.print(-1);
                pw.close();
            }
            Scanner s = new Scanner(hashFile);
            c = s.nextInt();
            if (hashingTotal != c) {
                PrintWriter pw = new PrintWriter(hashFile);
                pw.print(hashingTotal);
                pw.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("Finished; time elapsed: " + Double.toString((System.currentTimeMillis() - initTime)
                / (double) 1000) + " s\n_____\n");

        if (hashingTotal != c) {
            initTime = System.currentTimeMillis();
            System.out.println("_____\nPacking images...");
            packImages(atlasExportedImagesFolder);
            System.out.println("Finished; time elapsed: " + Double.toString((System.currentTimeMillis() - initTime)
                    / (double) 1000) + " s\n_____\n");
        }
    }

    private static int hashAllFiles(File f) {
        int total = 0;
        if (f.isDirectory()) {
            for (File content : f.listFiles()) {
                total += hashAllFiles(content);
            }
        } else {
            try {
                FileInputStream in = new FileInputStream(f.getPath());
                int c;
                while ((c = in.read()) != -1) {
                    total += c;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return total;
    }

    private static void packImages(String imagesFolder) {
        TexturePacker.process(imagesFolder, "atlas", "atlas");
    }
}

